// import modules
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('simple-mongoose-autoincrement');

// create artist schema
const ArtistSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  artist_id: {
    type: Number,
  },
  name: {
    type: String,
    trim: true,
    minlength: [2, 'Name must be at-least 2 characters long.']
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
  },
  phone: {
    type: String,
    trim: true,
    default: '',
    maxlength: [16, 'Phone can be max 15 characters long.']
  },
  avatar: {
    type: String,
    default: '',
  },
  spotify: {
    type: Object,
    id: {
      type: String,
      default: null,
    },
    name: {
      type: String,
      default: '',
    },
    url: {
      type: String,
      default: '',
    },
    image: {
      type: String,
      default: '',
    },
    genres: {
      type: [String],
      default: [],
    },
  },
});

ArtistSchema.plugin(autoIncrement, {field: 'artist_id'});

module.exports = Artist = mongoose.model('artists', ArtistSchema);

/*
  #### Fields:-
  * _id´
  * date´ | Date
  * artist_id´ | Number
  * name | String
  * email | String
  * phone | String
  * avatar | String
  * spotify | Object
    * id | String
    * name | String
    * url | String
    * image | String
    * genres | [String]
*/
