// import modules
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('simple-mongoose-autoincrement');

// create user schema
const UserSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  user_id: {
    type: Number,
  },
  name: {
    type: String,
    required: [true, 'Name/Company is required.'],
    trim: true,
    minlength: [2, 'Name must be at-least 2 characters long.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    trim: true,
    lowercase: true,
    unique: true
  },
  password: {
    type: String,
    // make password field not required to save spotify data without password
    // required: [true, 'Password is required.'],
  },
  phone: {
    type: String,
    trim: true,
    default: '',
    maxlength: [16, 'Phone can be max 15 characters long.']
  },
  avatar: {
    type: String,
    default: ''
  },
  reset_password_token: {
    type: String,
    default: ''
  },
  reset_password_expires: {
    type: Date,
  },
  role: {
    type: String,
    enum: ['admin', 'moderator', 'creator', 'guest', 'block'],
    default: 'creator'
  },
});

UserSchema.plugin(autoIncrement, {field: 'user_id'});

module.exports = User = mongoose.model('users', UserSchema);

/*
  #### Fields:-
  * _id´
  * date´ | Date
  * user_id´ | Number
  * name* | String
  * email* | String
  * password* | String
  * phone | String
  * avatar | String
  * reset_password_token | String
  * reset_password_expires | Date
  * role´ | String
*/
