// import modules
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('simple-mongoose-autoincrement');

// create event schema
const EventSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  event_id: {
    type: Number,
  },
  author: {
    user_id: {
      type: Number,
      required: [true, 'Event author details are missing.'],
    },
    name: {
      type: String,
      required: [true, 'Event author details are missing.'],
    },
    avatar: {
      type: String,
      trim: true,
      default: '',
    },
    role: {
      type: String,
      required: [true, 'Event author details are missing.'],
    },
  },
  venue_ref: {
    type: Schema.Types.ObjectId,
    ref: 'venues',
    required: [true, 'Venue Name is required.'],
  },
  name: {
    type: String,
    required: [true, 'Event Name is required.'],
    trim: true,
    minlength: [2, 'Name must be at-least 2 characters long.']
  },
  description: {
    type: String,
    required: [true, 'Event Description is required.'],
    minlength: [2, 'Description must be at-least 2 characters long.']
  },
  start_date: {
    type: Date,
    required: [true, 'Event start date is required.'],
  },
  start_time: {
    type: Date,
    required: [true, 'Event start time is required.'],
  },
  end_date: {
    type: Date,
    default: null
  },
  end_time: {
    type: Date,
    default: null
  },
  line_up: {
    type: [String],
    default: []
  },
  media_library: {
    type: [String],
    default: []
  },
  tags: {
    type: [String],
    default: []
  },
  source_url: {
    type: String,
    default: ''
  },
  status: {
    type: Boolean,
    default: false
  }
});

EventSchema.plugin(autoIncrement, {field: 'event_id'});

module.exports = Event = mongoose.model('events', EventSchema);

/*
#### Fields:-
  * _id´
  * date´ | Date
  * event_id´ | Number
  * user_ref* | Object
  * venue_ref* | Object
  * name* | String
  * description* | String
  * event_date* | Date
  * start_time* | Date
  * end_time | Date
  * line_up | [String]
  * media_library | [String]
  * tags | [String]
  * source_url | String
  * status´ | Boolean
*/