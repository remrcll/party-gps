// import modules
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('simple-mongoose-autoincrement');

// create venue schema
const VenueSchema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  venue_id: {
    type: Number,
  },
  name: {
    type: String,
    required: [true, 'Venue Name is required.'],
    trim: true,
    minlength: [2, 'Name must be at-least 2 characters long.']
  },
  display: {
    type: String,
    trim: true,
  },
  address: {
    line1: {
      type: String,
      required: [true, 'Address: line1 is required.'],
      trim: true,
      minlength: [2, 'Address: line1 must be at-least 2 characters long.'],
    },
    line2: {
      type: String,
      trim: true,
      default: ''
    },
    postal: {
      type: Number,
      required: [true, 'Postal is required.'],
      trim: true
    },
    city: {
      type: String,
      required: [true, 'City is required.'],
      trim: true
    },
    country: {
      type: String,
      required: [true, 'Country is required.'],
      trim: true
    }
  },
  geometry: {
    type: [Number, Number],
    index: '2d',
    required: [true, 'Coordinates are required.'],
    trim: true
  },
  phone: {
    type: String,
    trim: true,
    default: '',
    maxlength: [16, 'Phone can be max 15 characters long.']
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
  },
  avatar: {
    type: String,
    default: ''
  },
  website: {
    type: String,
    default: ''
  },
  description: {
    type: String,
    default: ''
  },
  category: {
    type: [String],
    default: []
  },
  tags: {
    type: [String],
    default: []
  },
  status: {
    type: Boolean,
    default: true
  },
  social_media: {
    facebook: {
      type: String,
      default: '',
    },
    instagram: {
      type: String,
      default: '',
    },
    twitter: {
      type: String,
      default: '',
    },
    youtube: {
      type: String,
      default: ''
    },
  },
});

VenueSchema.plugin(autoIncrement, {field: 'venue_id'});

module.exports = Venue = mongoose.model('venues', VenueSchema);

/*
  #### Fields:-
  * _id´
  * date´ | Date
  * venue_id´ | Number
  * name* | String
  * display | String
  * address* | String
    * line1* | String
    * line2 | String
    * postal* | Number
    * city* | String
    * country* | String
  * geometry* | [Number,Number]
  * phone | Number
  * email | String
  * avatar | String
  * website | String
  * description | String
  * category | [String]
  * tags | [String]
  * status´ | Boolean,
  * social_media | { {facebook | String}, {instagram | String}, {twitter | String}, {youtube | String}, }
*/
