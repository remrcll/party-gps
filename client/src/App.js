import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';
import {Provider} from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {setCurrentUser, logoutUser} from "./actions/authActions";

import './App.scss';

import setAuthToken from './utils/setAuthToken';
import store from './store';

import PrivateRoute from './components/common/PrivateRoute';
import MapView from "./components/map/MapView";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Dashboard from "./components/layout/Dashboard";
import Spotify from "./components/layout/Spotify";
import Venues from "./components/venue/Venues";
import Events from "./components/event/Events";
import VenuePage from "./components/venue/VenuePage";
import VenueEditPage from "./components/venue/VenueEditPage";
import EventPage from "./components/event/EventPage";
import EventEditPage from "./components/event/EventEditPage";
import AddVenue from "./components/venue/AddVenue";
import AddEvent from "./components/event/AddEvent";
import Import from "./components/import/Import";
import PageNotFound from "./components/common/PageNotFound";

// check for token in local storage
if(localStorage.jwtToken) {
  // set auth token in header auth
  setAuthToken(localStorage.jwtToken);
  // decode token and get user data
  const decodedData = jwt_decode(localStorage.jwtToken);
  // set current user and isAuthenticated
  store.dispatch(setCurrentUser(decodedData));

  // check for expired token
  const currentTime = Date.now() / 1000;
  if(decodedData.exp < currentTime) {
    // logout user
    store.dispatch(logoutUser());
    // redirect to login page
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Switch>
              <Route exact path="/" component={MapView} />
              <Route exact path="/login" component={Login}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/spotify" component={Spotify}/>
              <PrivateRoute exact path="/dashboard" component={Dashboard}/>
              <PrivateRoute exact path="/venues" component={Venues}/>
              <PrivateRoute exact path="/events" component={Events}/>
              <PrivateRoute exact path="/venues/add" component={AddVenue}/>
              <PrivateRoute exact path="/events/add" component={AddEvent}/>
              <PrivateRoute exact path="/venue/edit/:venue_id" component={VenueEditPage}/>
              <PrivateRoute exact path="/event/edit/:event_id" component={EventEditPage}/>
              <PrivateRoute exact path="/events/import" component={Import}/>
              <Route exact path="/venue/:venue_id" component={VenuePage}/>
              <Route exact path="/event/:event_id" component={EventPage}/>
              <Route exact component={PageNotFound}/>
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
