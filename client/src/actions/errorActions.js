import {GET_ERRORS} from "./types";

// clear all errors
export const clearErrors = () => {
  return {
    type: GET_ERRORS,
    payload: {}
  }
}