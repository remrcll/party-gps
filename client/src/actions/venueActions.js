import axios from "axios/index";

import {GET_ERRORS, VENUES_LOADING, GET_VENUES, GET_VENUE} from "./types";

// set venues loading
export const setVenuesLoading = () => {
  return {
    type: VENUES_LOADING
  }
}

// get all venues
export const getVenues = () => dispatch => {
  dispatch(setVenuesLoading());
  axios.get('/api/venue')
    .then(res => {
      dispatch({
        type: GET_VENUES,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_VENUES,
        payload: null
      })
    });
}

// get venue by venue_id
export const getVenue = (venue_id) => dispatch => {
  dispatch(setVenuesLoading());
  axios.get(`/api/venue/${venue_id}`)
    .then(res => {
      dispatch({
        type: GET_VENUE,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_VENUE,
        payload: null
      })
    });
}

// add new venue
export const addVenue = (userData, history) => dispatch => {
  dispatch(setVenuesLoading());
  axios.post('/api/venue', userData)
    .then(res => {
      dispatch({
        type: GET_ERRORS,
        payload: res.data
      });
      history.push('/venues')
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
}

// edit venue
export const editVenue = (userData) => dispatch => {
  dispatch(setVenuesLoading());
  axios.post('/api/venue/edit', userData)
      .then(res => {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        });
        return dispatch(getVenue(userData.venue_id));
      })
      .catch(err => {
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        });
      });
}

// delete a venue
export const deleteVenue = (venue_id) => dispatch => {
  if(window.confirm('Are you sure?')){
    axios.delete(`/api/venue/${venue_id}`)
      .then(res => {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        });
        return dispatch(getVenues());
      })
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
  }
}
