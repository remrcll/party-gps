export const GET_ERRORS = 'GET_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const VENUES_LOADING = 'VENUES_LOADING';
export const GET_VENUES = 'GET_VENUES';
export const GET_VENUE = 'GET_VENUE';
export const EVENTS_LOADING = 'EVENTS_LOADING';
export const GET_EVENTS = 'GET_EVENTS';
export const GET_EVENT = 'GET_EVENT';
export const USERS_LOADING = 'USERS_LOADING';
export const GET_USERS = 'GET_USERS';