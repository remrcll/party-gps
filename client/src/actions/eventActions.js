import axios from "axios/index";

import {GET_ERRORS, EVENTS_LOADING, GET_EVENTS, GET_EVENT} from "./types";
// import {getVenue} from "./venueActions";

// set events loading
export const setEventsLoading = () => {
  return {
    type: EVENTS_LOADING
  }
}

// get all events
export const getEvents = () => dispatch => {
  dispatch(setEventsLoading());
  axios.get('/api/event')
    .then(res => {
      dispatch({
        type: GET_EVENTS,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_EVENTS,
        payload: null
      })
    });
}

// get event by event_id
export const getEvent = (event_id) => dispatch => {
  dispatch(setEventsLoading());
  axios.get(`/api/event/${event_id}`)
    .then(res => {
      dispatch({
        type: GET_EVENT,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_EVENT,
        payload: null
      })
    });
}

// get events tagged by venue_id
export const getVenueTaggedEvents = (venue_id) => dispatch => {
  dispatch(setEventsLoading());
  axios.get(`/api/event/v/${venue_id}`)
      .then(res => {
        dispatch({
          type: GET_EVENTS,
          payload: res.data
        })
      })
      .catch(err => {
        dispatch({
          type: GET_EVENTS,
          payload: {}
        })
      });
}

// add new event
export const addEvent = (userData, history) => dispatch => {
  dispatch(setEventsLoading());
  axios.post('/api/event', userData)
    .then(res => {
      history.push('/events')
      // making state errors empty if they exit before successful event creation
      dispatch({
        type: GET_ERRORS,
        payload: {}
      })
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
}

// edit event
export const editEvent = (userData) => dispatch => {
  dispatch(setEventsLoading());
  axios.post('/api/event/edit', userData)
      .then(res => {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        });
        return dispatch(getEvent(userData.event_id));
      })
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
}

// delete an event
export const deleteEvent = (event_id) => dispatch => {
  if(window.confirm('Are you sure?')){
    axios.delete(`/api/event/${event_id}`)
      .then(res => {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        });
        return dispatch(getEvents());
      })
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
  }
}
