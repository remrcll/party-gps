import axios from "axios";

import {GET_ERRORS, USERS_LOADING, GET_USERS} from "./types";

// set users loading
export const setUsersLoading = () => {
  return {
    type: USERS_LOADING
  }
}

// get all users
export const getUsers = () => dispatch => {
  dispatch(setUsersLoading());
  axios.get('/user/all')
    .then(res => {
      dispatch({
        type: GET_USERS,
        payload: res.data
      })
    })
    .catch(err => {
      dispatch({
        type: GET_USERS,
        payload: null
      })
    });
}

// delete a user by user_id
export const deleteUser = (user_id)  => dispatch => {
  if(window.confirm('Are you sure?')) {
    axios.delete(`/user/${user_id}`)
      .then(res => {
        dispatch({
          type: GET_ERRORS,
          payload: res.data
        });
        return dispatch(getUsers());
      })
      .catch(err => {
        console.log('error in deleting user.');
        dispatch({
          type: GET_ERRORS,
          payload: null
        });
      });
  }
}
