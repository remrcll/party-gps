import {combineReducers} from 'redux';
import errorsReducer from './errorsReducer';
import authReducer from './authReducer';
import venuesReducer from './venuesReducer';
import eventsReducer from './eventsReducer';
import usersReducer from './usersReducer';

export default combineReducers({
  errors: errorsReducer,
  auth: authReducer,
  venues: venuesReducer,
  events: eventsReducer,
  users: usersReducer
});