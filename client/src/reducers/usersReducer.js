import {USERS_LOADING, GET_USERS} from "../actions/types";

const initialState = {
  users: null,
  loading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case USERS_LOADING:
      return {
        ...state,
        loading: true
      }
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
        loading: false
      }
    default:
      return state;
  }
}