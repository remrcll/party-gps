import {VENUES_LOADING, GET_VENUES, GET_VENUE} from "../actions/types";

const initialState = {
  venues: null,
  venue: null,
  loading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case VENUES_LOADING:
      return {
        ...state,
        loading: true
      }
    case GET_VENUES:
      return {
        ...state,
        venues: action.payload,
        loading: false
      }
    case GET_VENUE:
      return {
        ...state,
        venue: action.payload,
        loading: false
      }
    default:
      return state;
  }
}