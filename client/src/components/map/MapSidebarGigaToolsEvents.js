import React, {Component} from 'react';
import {/*Link, */withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getEvents} from "../../actions/eventActions";
import Moment from 'react-moment';
import moment from 'moment';

import sidebarHeaderVenue from '../../assets/sidebar_header_venue.jpg';

class MapSidebarGigaToolsEvents extends Component {
    constructor(props) {
        super(props);

        this.state = {
            today: new Date(),
        }
    }

    render() {
        const {today} = this.state;
        const {data, venueEvents} = this.props;
        let todayEvent = false;
        let todayEventData = null;
        let upcomingEventsHtml = (<div>Coming Soon...</div>);

        if (venueEvents.length > 0) {
            // console.log((events[0].start_date));

            // check today date
            // todayEvent = (moment((events[0].start_date)).isSame(moment(), 'day'))
            // if(todayEvent) {
            //   todayEventData = events[0];
            // }
            todayEvent = null;

            upcomingEventsHtml = venueEvents.map((event, index) =>
                (
                    <div className="each-event-container d-flex align-items-center pb-2" key={index}>
                        <i className="fas fa-calendar-check pr-3"></i>
                        <div className="each-event-text">
                            <div className="each-event-title">{event.name}</div>
                            <div className="event-date">{event.eventdate}</div>
                            <div className="door-close">{event.showtime}</div>
                        </div>
                    </div>
                )
            );
        }

        // sidebar header (buttons and title)
        let sidebarHeaderHtml = (
            <header className="sidebar-header-container p-3 d-flex flex-wrap justify-content-between">
                <div className="back-btn"><i className="fas fa-arrow-left"></i></div>
                <div className="venue-title text-capitalize font-weight-bold">{data.name}</div>
                <div className="menu-btn"><i className="fas fa-bars"></i></div>
            </header>
        )

        // sidebar footer (address)
        let sidebarFooterHtml = (
            <footer className="sidebar-footer-container py-3 px-2 text-white mt-auto">
                <div className="address-container d-flex align-items-center align-items-stretch">
                    <i className="fas fa-map-marker-alt fa-2x pr-3 d-flex align-items-center"></i>
                    <div className="venue-address d-flex flex-wrap align-items-center ">
                        {(data.address.line1) && (<span className="pr-2">{data.address.line1},</span>)}
                        {(data.address.line2) && (<span className="pr-2">{data.address.line2},</span>)}
                        {(data.address.postal) && (<span className="pr-2">{data.address.postal},</span>)}
                        {(data.address.city) && (<span className="pr-2">{data.address.city}.</span>)}
                    </div>
                </div>
            </footer>
        )

        let sidebarHeaderImageHtml = (
            <div className="sidebar-header-image-container"><img className="card-img-top"
                                                                 src={(data.avatar === '') ? sidebarHeaderVenue : data.avatar}
                                                                 alt="Card cap"/></div>
        );

        // sidebar content
        let sidebarContentHtml = (
            <div className="sidebar-content-container border-0 bg-white">

                <div className="content-body-wrapper p-3">
                    {todayEvent &&
                    (<React.Fragment>
                        <div className="content-happening pb-2">
                            Today/RightNow
                        </div>

                        <div className="content-title font-weight-bold pb-4">Today Event Name</div>

                        <div className="content-date-time d-flex align-items-center pb-4">
                            <i className="far fa-clock fa-2x pr-3"></i>
                            <div className="date-time-text">
                                <div className="door-open">Doors Open: <Moment format="dddd @ HH:mm">Today Event Start
                                    Time</Moment></div>
                                <div className="door-close">Doors Close: <Moment format="dddd @ HH:mm">Today Event End
                                    Time</Moment></div>
                            </div>
                        </div>

                        <div className="content-description pb-2">
                            <div className="description-label">
                                <a className="text-dark font-weight-bold" data-toggle="collapse"
                                   href="#collapseDescription" role="button" aria-expanded="false"
                                   aria-controls="collapseExample"
                                >
                                    <i className="fas fa-chevron-down pr-3"></i>
                                    Description
                                </a>
                            </div>
                            <div className="collapse" id="collapseDescription">
                                {todayEventData && todayEventData.description}
                            </div>
                        </div>

                        <div className="content-lineup pb-2">
                            <div className="lineup-label">
                                <a className="text-dark font-weight-bold" data-toggle="collapse"
                                   href="#collapseLineup" role="button" aria-expanded="false"
                                   aria-controls="collapseExample"
                                >
                                    <i className="fas fa-chevron-down pr-3"></i>
                                    Line up
                                </a>
                            </div>
                            <div className="collapse" id="collapseLineup">
                                {todayEventData && todayEventData.description}
                            </div>
                        </div>
                    </React.Fragment>)}

                    <div className="content-upcoming pt-2">
                        <div className="upcoming-label font-weight-bold pb-2">Upcoming Events</div>
                        <div className="upcoming-events-text">
                            {upcomingEventsHtml}
                        </div>
                    </div>
                </div>

            </div>
        );

        return (
            <div className="sidebar container d-flex flex-column">
                {sidebarHeaderHtml}
                {sidebarHeaderImageHtml}
                {sidebarContentHtml}
                {sidebarFooterHtml}
            </div>
        );
    }
}

MapSidebarGigaToolsEvents.propTypes = {
    getEvents: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    events: state.events,
});

export default connect(mapStateToProps, {getEvents})(withRouter(MapSidebarGigaToolsEvents));
