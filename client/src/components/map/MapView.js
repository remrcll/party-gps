// default imports
import React, {Component} from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

// map & icons imports
import {Map, Marker, Popup, TileLayer} from 'react-leaflet';
import logo from '../../assets/logo.svg';
import venueIcon from '../../assets/venue_location_icon.png';
import eventIcon from '../../assets/event_location_icon.png';
import {CurrentLocationIcon} from "../common/CurrentLocationIcon";
import MapSidebar from "./MapSidebar";
import MapSidebarGigaToolsEvents from "./MapSidebarGigaToolsEvents";

// api functions & others imports
import {getVenues} from "../../actions/venueActions";
import {getEvents} from "../../actions/eventActions";
import {getVenueTaggedEvents} from "../../actions/eventActions";

// map styling
import './MapView.scss';
import './Sidebar.scss';

class MapView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current_location: {lat: 52.516399, lng: 13.377736},
            have_current_location: false,
            zoom: 12,
            venues: [],
            events: [],
            venueEvents: [],
            showVenues: true,
            showEvents: true,
            showSidebar: false,
            sidebarData: {}
        }
    }

    componentDidMount() {
        this.props.getVenues();
        this.props.getEvents();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.venues.venues) {
            const venues = nextProps.venues.venues;
            this.setState({venues});
        }

        if (nextProps.events.events) {
            const events = nextProps.events.events;
            this.setState({events});
        }
    }

    currentLocation = e => {
        navigator.geolocation.getCurrentPosition(position => {
            // console.log('user gave permission to access location.');
            this.setState({
                current_location: {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                },
                have_current_location: true,
                zoom: 14
            });
        }, () => {
            // console.log('user didn\'t gave access to show location.');
            axios.get('https://ipapi.co/json')
                .then(res => {
                    this.setState({
                        current_location: {
                            lat: res.data.latitude,
                            lng: res.data.longitude
                        },
                        have_current_location: true,
                        zoom: 14
                    });
                })
                .catch(err => console.log(err));
        });
    }

    mapClick = e => {
        this.setState({showSidebar: false});
    }

    markerClick = (venueData) => {
        const venue = venueData.name;
        const events = this.state.events;
        let venueEvents = events.filter(function (event) {
            return (event.venue === venue);
        });
        this.setState({
            showSidebar: true,
            sidebarData: venueData,
            venueEvents: venueEvents,
        });
    }

    render() {
        const {isAuthenticated} = this.props.auth;
        const {current_location, have_current_location, zoom, venues, venueEvents, showVenues, showEvents, showSidebar, sidebarData} = this.state;
        let venues_marker, current_location_marker;

        // creating venues markers
        if (venues.length) {
            venues_marker = (venues).map((venue, index) =>
                (<Marker key={index} position={venue.geometry} onClick={this.markerClick.bind(this, venue)}/>)
            );
        }

        // creating current location marker
        if (have_current_location) {
            current_location_marker = (
                <Marker position={current_location} icon={CurrentLocationIcon}>
                    <Popup>
                        your current location!
                    </Popup>
                </Marker>);
        }


        return (
            <div id="map-view">
                {/*full-page map*/}
                <Map center={current_location} zoom={zoom} onClick={this.mapClick}>
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                    />
                    {current_location_marker}
                    {showVenues && venues_marker}
                </Map>

                {/*buttons zoom_in ,zoom_out , current_location, venues, events, dashboard*/}
                <div className="container-current-location-button">
                    <button className="btn current-location-button" onClick={this.currentLocation}><img src={logo}
                                                                                                        alt="current location"/>
                    </button>
                    <button className="btn show-hide-venues" onClick={() => this.setState({showVenues: !showVenues})}>
                        <img src={venueIcon} alt="current location"/></button>
                    <button className="btn show-hide-event" onClick={() => this.setState({showEvents: !showEvents})}>
                        <img src={eventIcon} alt="current location"/></button>
                    {isAuthenticated && <button className="btn btn-dashboard"><Link to='/dashboard'><i
                        className="fas fa-chart-line"></i></Link></button>}
                    {!isAuthenticated &&
                    <button className="btn btn-login"><Link to='/login'><i className="fas fa-sign-in-alt"></i></Link>
                    </button>}
                </div>

                {/*sidebar*/}
                {showSidebar &&
                <div className="sidebar-wrapper position-absolute bg-white">
                    {/*<MapSidebar data={sidebarData}/>*/}
                    <MapSidebarGigaToolsEvents data={sidebarData} venueEvents={venueEvents}/>
                </div>
                }
            </div>
        );
    }
}

MapView.propTypes = {
    auth: PropTypes.object.isRequired,
    getVenues: PropTypes.func.isRequired,
    getEvents: PropTypes.func.isRequired,
    getVenueTaggedEvents: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    venues: state.venues,
    events: state.events,
});

export default connect(mapStateToProps, {getVenues, getEvents, getVenueTaggedEvents})(withRouter(MapView));
