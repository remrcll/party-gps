import React, {Component} from 'react';
import ical2json from 'ical2json';
import axios from 'axios';
import Moment from "react-moment";
import Navbar from "../layout/Navbar";

class Import extends Component {
  state = {
    icsUploadedData: [],
    events: [],
    max_event_id: null,
    showDownloadButton: false,
  }

  updateEvents = (events) => {
    let data = [];
    let event_id = this.state.max_event_id;

    if(event_id) {
      events.map((event) => {
        event_id++;
        const eventObject = {
          start_date: event.DTSTART,
          start_time: event.DTSTART,
          end_date: event.DTEND,
          end_time: event.DTEND,
          name: event.SUMMARY,
          description: event.DESCRIPTION.replace("\\n", '').replace("\\n\\n", ''),
          venue_name: event.LOCATION,
          source_url: event.URL,
          event_id: event_id,
        }
        data.push(eventObject);
        return;
      })

      this.setState({
        events: data,
        showDownloadButton: true,
      });
    }
  }

  icsToJsonConversion = (filePath) => {
    const _this = this;
    const file = new XMLHttpRequest();
    file.open("GET", filePath, false);
    file.onreadystatechange = () => {
      if(file.readyState === 4)
      {
        if(file.status === 200 || file.status === 0)
        {
          const icsFileData = file.responseText;
          const outputJSON = ical2json.convert(icsFileData);
          const yourEventsArray = outputJSON.VCALENDAR[0].VEVENT;
          _this.setState({icsUploadedData: yourEventsArray});
          this.updateEvents(yourEventsArray);
        }
      }
    }
    file.send(null);
  }

  getMaxVenueIdValue = () => {
    axios.get('/api/event/max/id')
      .then(res => {
        const max_event_id = res.data;
        if(max_event_id) {
          this.setState({
            max_event_id
          })
        }
      })
      .catch(err => console.log(err));
  }

  componentDidMount() {
    this.getMaxVenueIdValue();
  }

  chosenFile = e => {
    e.preventDefault();
    const filePath = URL.createObjectURL(e.target.files[0]);
    this.icsToJsonConversion(filePath);
  }

  downloadJson = () => {
    let filename = "events.json";
    let contentType = "application/json;charset=utf-8;";
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      let blob = new Blob([decodeURIComponent(encodeURI(JSON.stringify(this.state.events)))], { type: contentType });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      let a = document.createElement('a');
      a.download = filename;
      a.href = 'data:' + contentType + ',' + encodeURIComponent(JSON.stringify(this.state.events));
      a.target = '_blank';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
    this.setState({showDownloadButton: false});
    window.location.reload();
  }

  render() {
    const {events, showDownloadButton} = this.state;

    let html;
    if(events) {
      html = (events).map((event, index) =>
          (<div className="card mb-2" key={index}>
            <div className="card-header">
              {event.event_id}: {event.name}
            </div>
            <div className="card-body">
              <p className="event-description"><i className="fas fa-building pr-2"></i>{event.venue_name}</p>
              <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{event.start_date}</Moment></p>
              <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{event.start_time}</Moment></p>
              <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{event.end_date}</Moment></p>
              <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{event.end_time}</Moment></p>
              <p className="event-description">{event.description}</p>
              <a href={event.source_url} target="_blank" rel="noopener noreferrer" className="btn btn-primary">Event Source URL</a>
            </div>
          </div>)
      );
    }

    return (
        <div id='facebook-events'>
          <Navbar/>

          <div className="container">

            <div className="card my-3">
              <h5 className="card-header">Import Facebook Events</h5>
              <div className="card-body">
                <h5 className="card-title">Convert .ics to .json</h5>
                <div className="card-text pb-3">
                  <ol className="list-group">
                    <li className="list-group-item">1. Click Events on the left side of your homepage under Explore.</li>
                    <li className="list-group-item">2. On the bottom right side of the page under Upcoming Birthdays, click Upcoming Events to download upcoming events to your computer. Click Birthdays to download friends' birthdays to your computer. The downloaded calendar file is supported across Microsoft Outlook, Google Calendar or Apple Calendar.</li>
                    <li className="list-group-item">3. Open the downloaded file in your calendar of choice.</li>
                    <li className="list-group-item">4. Choose the downloaded .ics file from the given below choose file input field.</li>
                    <li className="list-group-item">5. After step 4 Download button will apear, by clicking it you can download events.json file.</li>
                  </ol>
                </div>

                <form className="import-events-ics-file-form" onSubmit={(e) => e.preventDefault()}>
                  <div className="form-row d-flex justify-content-between">
                    <div className="form-group col-md-4 bg-dark text-white rounded py-2">
                      <input
                          type="file"
                          name="events_ics_file"
                          className="input-lg"
                          onChange={this.chosenFile}
                          accept=".ics"
                      />
                    </div>

                    <div className="form-group col-md-4 px-0">
                      {showDownloadButton && <button className="btn btn-large btn-block btn-danger download-btn" style={{'height': '100%'}} onClick={this.downloadJson}>Download</button>}
                    </div>
                  </div>
                </form>

              </div>
            </div>

            <div className="imported-facebook-events-card">
              {html}
            </div>
          </div>
        </div>
    );
  }
}

export default Import;
