import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import { Link } from 'react-router-dom';
import {withRouter} from "react-router-dom";
import {getEvent} from "../../actions/eventActions";
import Loading from "../common/Loading";
import Moment from "react-moment";
import Navbar from "../layout/Navbar";
import isEmpty from "../../utils/isEmpty";

class EventPage extends Component {
  componentDidMount() {
    const event_id = this.props.match.params.event_id;
    this.props.getEvent(event_id);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.events.event === null && this.props.events.loading) {
      this.props.history.push('/pagenotfound');
    }
  }

  render() {
    const {event, loading} = this.props.events;
    const {isAuthenticated, user: {role}} = this.props.auth;

    let html;
    if(event && !loading) {
      const {event_id, name, description, start_date, start_time, end_date, end_time, tags, source_url, author} = event;
      const {line1, line2, postal, city, country} = event.venue_ref.address;
      const {venue_id} = event.venue_ref;
      const venue_name = event.venue_ref.name;

      html = (
        <div>
          {(isAuthenticated)
              ?
              (<Link to='/events'><i className="fas fa-arrow-circle-left pr-2 py-3"></i>Events</Link>)
              :
              (<Link to='/'><i className="fas fa-arrow-circle-left pr-2 py-3"></i>Go Back</Link>)
          }

          <h1 className="">{name}</h1>
          {venue_name && <p className="event-venue-name"><i className="fas fa-building pr-2"></i><Link to={`/venue/${venue_id}`}>{venue_name}</Link></p>}
          <p className="event-address">
            <i className="fas fa-map-marker-alt pr-2"></i>
            {line1 && line1+', '}
            {line2 && line2+', '}
            {postal && postal+', '}
            {city} {country}
          </p>
          {start_date && <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{start_date}</Moment></p>}
          {start_time && <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{start_time}</Moment></p>}
          {end_date && <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{end_date}</Moment></p>}
          {end_time && <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{end_time}</Moment></p>}
          {author && <p className="event-venue-name"><i className="fas fa-user-edit pr-2"></i>{author.name}</p>}
          {description && <p className="event-description">{description}</p>}
          {(tags) && (
              <div className="event-category" id="display-category">
                {tags && (tags.map((tag, index) => (<span key={index} className='tag-wrapper tag-bg'>{tag}</span>)))}
              </div>
          )}
          <div className="d-flex pt-2 venue-social-media">
            {(role === 'admin' || role === 'moderator') && <Link to={`/event/edit/${event_id}`}><i className="far fa-edit text-info pr-3" style={{'fontSize': '30px'}}></i></Link>}

            {isEmpty(source_url) ? null : (
                <a className="pr-3" href={source_url} target="_blank" rel="noopener noreferrer">
                  <i className="fas fa-globe website" style={{'fontSize': '30px'}}></i>
                </a>
            )}
          </div>
        </div>
      )
    } else {
      html = <Loading/>;
    }

    return (
      <div id="event-page">
        <Navbar/>

        <div className="container">
          {html}
        </div>
      </div>
    );
  }
}

EventPage.propTypes = {
  auth: PropTypes.object.isRequired,
  events: PropTypes.object.isRequired,
  getEvent: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  events: state.events,
});

export default connect(mapStateToProps, {getEvent})(withRouter(EventPage));
