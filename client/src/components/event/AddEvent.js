import React, {Component} from 'react';
import classnames from "classnames";
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {getVenues} from "../../actions/venueActions";
import {addEvent} from "../../actions/eventActions";
import Select from 'react-select';
import Datetime from 'react-datetime';
import 'react-datetime/css/react-datetime.css'
import Navbar from "../layout/Navbar";
import {WithContext as ReactTags} from "react-tag-input";
import InputGroup from "../common/InputGroup";
import defaultEventImage from '../../assets/sidebar_header_event.jpg';
import isEmpty from "../../utils/isEmpty";

const KeyCodes = {
  comma: 188,
  enter: 13,
  space: 32,
  minus: 189,
  hash: 191
};

class AddEvent extends Component {
  state = {
    venues: [],
    venue_name: '',
    name: '',
    description: '',
    start_date: null,
    start_time: null,
    end_date: null,
    end_time: null,
    line_up: [],
    media_library: [],
    tags: [],
    source_url: '',
    errors: {}
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const {venue_name, name, description, start_date, start_time, end_date, end_time, line_up, media_library, tags, source_url} = this.state;

    let mediaUrls = [];
    if(isEmpty(media_library)) {
      mediaUrls[0] = defaultEventImage;
    } else {
      mediaUrls = media_library.split(',');
    }

    const userInput = {
      venue_name,
      name,
      description,
      start_date,
      start_time,
      end_date,
      end_time,
      line_up,
      media_library: mediaUrls,
      tags,
      source_url
    }

    this.props.addEvent(userInput, this.props.history);
  }

  onDeleteEventTag = (i) => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i),
    });
  }

  onChangeEventTags = (tag) => {
    this.setState(state => ({ tags: [...state.tags, '#'+tag.id.toLowerCase()] }));
  }

  componentDidMount() {
    this.props.getVenues();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.venues.venues !== this.props.venues.venues) {
      if(nextProps.venues.venues.length) {
        let venuesObjectArray = [];
        nextProps.venues.venues.forEach((eachVenue) => {
          let eachVenueObject = {}
          eachVenueObject['value'] = eachVenue.name;
          eachVenueObject['label'] = eachVenue.name;
          venuesObjectArray.push(eachVenueObject);
        });
        this.setState({venues: venuesObjectArray});
      }
    }

    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }
  }

  render() {
    const {name, description, start_date, start_time, end_date, end_time, /*line_up,*/ media_library, tags, source_url, errors} = this.state;

    let tagsObjectArray = [];
    if(tags) {
      tags.forEach((eachTag) => {
        let eachTagObject = {}
        eachTagObject['id'] = eachTag;
        eachTagObject['text'] = eachTag;
        tagsObjectArray.push(eachTagObject);
      })
    }

    return (
      <div id="add-event">
        <Navbar/>
        <div className="container p-0">
          <form name="login_form" className="form-style m-auto" noValidate onSubmit={this.onSubmit} autoComplete="off">
            <h3 className="form-login-heading text-center">Add Event</h3>
            <hr className="colorgraph mb-5" />

            <Select
              isClearable
              placeholder="Select Venue"
              name="venue_name"
              options={this.state.venues}
              onChange={(venue_name) => (venue_name) ? this.setState({venue_name: venue_name.value}) : this.setState({venue_name: ''})}
              className={classnames('basic-multi-select form-control border-0 p-0 pb-3', {'is-invalid': errors.venue_name})}
            />
            {errors.venue_name && (<div className="invalid-feedback pb-3">{errors.venue_name}</div>)}

            <div className="form-group">
              <input type="text" name="name" placeholder="Event Name" onChange={this.onChange} value={name} className={classnames('form-control', {'is-invalid': errors.name})}/>
              {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
            </div>

            <div className="form-group">
              <textarea rows="5" name="description" placeholder="Event description" onChange={this.onChange} value={description} className={classnames('form-control', {'is-invalid': errors.description})}/>
              {errors.description && (<div className="invalid-feedback">{errors.description}</div>)}
            </div>

            <div className="form-row event-date-time">
              <div className="form-group col-md-6">
                <label htmlFor="event-start-date" className="m-0">Event Start Date</label>
                <Datetime
                  onChange={(start_date) => this.setState({start_date})}
                  value={start_date}
                  timeFormat={false}
                  className={classnames('form-control p-0 border-0', {'is-invalid': errors.start_date})}
                  dateFormat="DD-MM-YYYY"
                  inputProps={{ placeholder: 'DD-MM-YYYY'}}
                />
                {errors.start_date && (<div className="invalid-feedback">{errors.start_date}</div>)}
              </div>

              <div className="form-group col-md-6">
                <label htmlFor="event-start-time" className="m-0">Event Start Time</label>
                <Datetime
                  onChange={(start_time) => this.setState({start_time})}
                  value={start_time}
                  dateFormat={false}
                  className={classnames('form-control p-0 border-0', {'is-invalid': errors.start_time})}
                  timeFormat="HH:mm"
                  inputProps={{ placeholder: '22:00'}}
                />
                {errors.start_time && (<div className="invalid-feedback">{errors.start_time}</div>)}
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="event-end-date" className="m-0">Event End Date</label>
                <Datetime
                  onChange={(end_date) => this.setState({end_date})}
                  value={end_date}
                  timeFormat={false}
                  className={classnames('form-control p-0 border-0', {'is-invalid': errors.end_date})}
                  dateFormat="DD-MM-YYYY"
                  inputProps={{ placeholder: 'DD-MM-YYYY'}}
                />
                {errors.end_date && (<div className="invalid-feedback">{errors.end_date}</div>)}
              </div>

              <div className="form-group col-md-6">
                <label htmlFor="event-end-time" className="m-0">Event End Time</label>
                <Datetime
                  onChange={(end_time) => this.setState({end_time})}
                  value={end_time}
                  dateFormat={false}
                  className={classnames('form-control p-0 border-0', {'is-invalid': errors.end_time})}
                  timeFormat="HH:mm"
                  inputProps={{ placeholder: '06:00'}}
                />
                {errors.end_time && (<div className="invalid-feedback">{errors.end_time}</div>)}
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="event-source-url" className="m-0">Event Source</label>
              <input type="url" pattern="https://.*" name="source_url" placeholder="Event Source Url i.e. https://venue.de" onChange={this.onChange} value={source_url} className={classnames('form-control', {'is-invalid': errors.source_url})}/>
              {errors.source_url && (<div className="invalid-feedback">{errors.source_url}</div>)}
            </div>

            <label htmlFor="event-image" className="m-0">Event Image i.e. Flyer</label>
            <InputGroup
                placeholder="Event Image URL"
                name="media_library"
                icon="far fa-image"
                value={media_library}
                onChange={this.onChange}
                error={errors.media_library}
            />

            <div className="form-group">
              <ReactTags
                  classNames={{
                    tagInputField: 'input-tags',
                  }}
                  id="event-tags-input"
                  tags={tagsObjectArray}
                  handleDelete={this.onDeleteEventTag}
                  handleAddition={this.onChangeEventTags}
                  delimiters={[KeyCodes.enter, KeyCodes.comma, KeyCodes.space, KeyCodes.minus, KeyCodes.hash]}
              />
            </div>

            <button className="btn btn-lg btn-primary btn-block my-4" type="Submit">Add Event</button>
          </form>
        </div>
      </div>
    );
  }
}

AddEvent.propTypes = {
  venues: PropTypes.object.isRequired,
  getVenues: PropTypes.func.isRequired,
  addEvent: PropTypes.func.isRequired,
  errors: PropTypes.object
}

const mapStateToProps = (state) => ({
  venues: state.venues,
  errors: state.errors
});

export default connect(mapStateToProps, {getVenues, addEvent})(withRouter(AddEvent));
