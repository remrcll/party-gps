import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import Loading from "../common/Loading";
import EventCard from "./EventCard";
import EventCardGigaTools from "./EventCardGigaTools";
import {getEvents, deleteEvent} from "../../actions/eventActions";
import {AlertList} from "react-bs-notifier";
import {clearErrors} from "../../actions/errorActions";
import Navbar from "../layout/Navbar";

class Events extends Component {
  state = {
    alerts: [],
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors.alert) {
      this.setState({
        alerts: [nextProps.errors.alert]
      });
    }
  }

  componentDidMount() {
    this.props.getEvents();
  }

  deleteEvent = (event_id) => {
    this.props.deleteEvent(event_id);
  }

  render() {
    const {events, loading} = this.props.events;
    const {role} = this.props.auth.user;
    const {alerts} = this.state;

    let html, totalEventsCount;
    if(events && !loading) {
      events.length === 0 ? totalEventsCount = ' No event record found.' : totalEventsCount = events.length;
      html = (events).map((event, index) =>
        // (<EventCard key={index} event={event} role={role} delete={this.deleteEvent.bind(this, event.event_id)}/>)
          (<EventCardGigaTools key={index} event={event}/>)
      );
    } else {
      html = <Loading/>;
    }

    return (
      <div id="events">
        {alerts &&
        (
            <div className="alert-wrapper">
              <AlertList
                  position="top-right"
                  alerts={alerts}
                  timeout={3000}
                  onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
              />
            </div>
        )
        }

        <Navbar/>

        <div className="container">
          {/*events count block*/}
          <div className="events-header d-flex justify-content-between align-items-center">
            <div className="lead py-3">
              Events:{totalEventsCount}
            </div>
            <Link to="/events/add"><i className="fas fa-plus px-2"></i>Event</Link>
          </div>

          {/*events card*/}
          <div id="events-cards">
            {html}
          </div>
        </div>
      </div>
    );
  }
}

Events.propTypes = {
  getEvents: PropTypes.func.isRequired,
  deleteEvent: PropTypes.func.isRequired,
  events: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  events: state.events,
  errors: state.errors,
});

export default connect(mapStateToProps, {getEvents, deleteEvent, clearErrors})(withRouter(Events));
