import React from 'react';
import {Link} from "react-router-dom";
import Moment from 'react-moment';

const EventCard = (props) => {
  const {event_id, name, start_date, start_time, end_date, end_time, tags, source_url} = props.event;
  const {line1, line2, postal, city, country} = props.event.venue_ref.address;
  const venue_name = props.event.venue_ref.name;
  const {role} = props;

  return (
    <div className="card mb-2" id={event_id}>
      <h5 className="card-header event-header d-flex justify-content-between">
        <div className="event-name"><Link to={`/event/${event_id}`}>{name}</Link></div>
        <div className="control-buttons">
          {source_url && <a href={source_url} className="event-source-url pl-2 text-success" aria-label="event source url" target="_blank" rel="noopener noreferrer"><i className="fas fa-globe"></i></a>}
          {(role === 'admin' || role === 'moderator') && <Link to={`/event/edit/${event_id}`}><i className="far fa-edit pl-2 edit"></i></Link>}
          {role === 'admin' && <i className="fas fa-trash-alt text-danger pl-2" id={event_id} onClick={props.delete}></i>}
        </div>
      </h5>
      <div className="card-body">
        {venue_name && <p className="event-venue-name"><i className="fas fa-building pr-2 venue"></i>{venue_name}</p>}
        {line1 &&
        <p className="event-venue-address"><i className="fas fa-map-marker-alt pr-2"></i>
          {line1 && line1+', '}
          {line2 && line2+', '}
          {postal && postal+', '}
          {city} {country}
        </p>}
        {start_date && <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{start_date}</Moment></p>}
        {start_time && <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{start_time}</Moment></p>}
        {end_date && <p className="event-start-date"><i className="fas fa-calendar-alt pr-2"></i><Moment format="DD-MM-YYYY">{end_date}</Moment></p>}
        {end_time && <p className="event-start-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{end_time}</Moment></p>}

        {(tags) && (
            <div className="event-category" id="display-category">
              {tags && (tags.map((tag, index) => (<span key={index} className='tag-wrapper tag-bg'>{tag}</span>)))}
            </div>
        )}
      </div>
    </div>
  );
};

export default EventCard;
