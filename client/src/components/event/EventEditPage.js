import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import PropTypes from 'prop-types';
import {clearErrors} from "../../actions/errorActions";
import {getEvent, editEvent} from "../../actions/eventActions";
import {withRouter} from "react-router-dom";
import {AlertList} from "react-bs-notifier";
import Datetime from 'react-datetime';
import isEmpty from "../../utils/isEmpty";
import defaultEventImage from "../../assets/sidebar_header_event.jpg";
import {WithContext as ReactTags} from "react-tag-input";
import classnames from "classnames";
import Navbar from '../layout/Navbar';
import InputGroup from "../common/InputGroup";

const KeyCodes = {
  comma: 188,
  enter: 13,
  space: 32,
  minus: 189,
  hash: 191
};

class EventEditPage extends Component {
  state = {
    event_id: '',
    venue_name: '',
    name: '',
    description: '',
    start_date: null,
    start_time: null,
    end_date: null,
    end_time: null,
    line_up: [],
    media_library: [],
    tags: [],
    source_url: '',
    errors: {},
    editForm: true,
    alerts: [],
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const {event_id, venue_name, name, description, start_date, start_time, end_date, end_time, line_up, media_library, tags, source_url} = this.state;

    let mediaUrls = [];
    if(isEmpty(media_library)) {
      mediaUrls[0] = defaultEventImage;
    } else if((typeof media_library) !== 'object') {
      mediaUrls = media_library.split(',');
    } else {
      mediaUrls = media_library;
    }

    const userInput = {
      event_id,
      venue_name,
      name,
      description,
      start_date,
      start_time,
      end_date,
      end_time,
      line_up,
      media_library: mediaUrls,
      tags,
      source_url
    }
    this.setState({editForm: true});

    this.props.editEvent(userInput);
  }

  onDeleteEventTag = (i) => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i),
    });
  }

  onChangeEventTags = (tag) => {
    this.setState(state => ({ tags: [...state.tags, '#'+tag.id.toLowerCase()] }));
  }

  componentDidMount() {
    const event_id = this.props.match.params.event_id;
    this.props.getEvent(event_id);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.events.event === null && this.props.events.loading) {
      this.props.history.push('/pagenotfound');
    }

    if(nextProps.errors.alert) {
      this.setState({
        alerts: [nextProps.errors.alert]
      });
    }

    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }

    if(nextProps.events.event) {
      // storing fetched venue data in a const venue
      const event = nextProps.events.event;

      // make fields that are not filled, equal to empty strings
      event.event_id = !isEmpty(event.event_id) ? event.event_id : '';
      event.venue_name = !isEmpty(event.venue_ref.name) ? event.venue_ref.name : '';
      event.name = !isEmpty(event.name) ? event.name : '';
      event.start_date = !isEmpty(event.start_date) ? event.start_date : null;
      event.start_time = !isEmpty(event.start_time) ? event.start_time : null;
      event.end_date = !isEmpty(event.end_date) ? event.end_date : null;
      event.end_time = !isEmpty(event.end_time) ? event.end_time : null;
      event.description = !isEmpty(event.description) ? event.description : '';
      event.source_url = !isEmpty(event.source_url) ? event.source_url : '';
      event.media_library = !isEmpty(event.media_library) ? event.media_library : [];
      event.tags = !isEmpty(event.tags) ? event.tags : [];

      // set all fields to the state
      this.setState({
        event_id: event.event_id,
        venue_name: event.venue_ref.name,
        name: event.name,
        description: event.description,
        start_date: event.start_date,
        start_time: event.start_time ,
        end_date: event.end_date,
        end_time: event.end_time,
        source_url: event.source_url,
        media_library: event.media_library,
        tags: event.tags,
      });

    }
  }

  render() {
    const {event_id, venue_name, name, description, start_date, start_time, end_date, end_time, /*line_up,*/ media_library, tags, source_url, errors, editForm, alerts} = this.state;

    let tagsObjectArray = [];
    if(tags) {
      tags.forEach((eachTag) => {
        let eachTagObject = {}
        eachTagObject['id'] = eachTag;
        eachTagObject['text'] = eachTag;
        tagsObjectArray.push(eachTagObject);
      })
    }

    return (
        <div id="event-edit-page">
          {alerts &&
          (
              <div className="alert-wrapper">
                <AlertList
                    position="top-right"
                    alerts={alerts}
                    timeout={3000}
                    onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
                />
              </div>
          )
          }

          <Navbar/>

          <div className="container p-0">
            <form name="edit-event-form" className="form-style m-auto" noValidate onSubmit={this.onSubmit} autoComplete="off">
              <h3 className="text-center">Edit Event</h3>
              <hr className="colorgraph mb-5" />

              <div className="form-group">
                <div className="form-control bg-dark text-white">EventID: {event_id}</div>
              </div>

              <div className="form-group">
                <div className="form-control bg-dark text-white">Venue: {venue_name}</div>
              </div>

              <div className="form-group">
                <div className="form-control bg-dark text-white">Event: {name}</div>
              </div>

              <div className="form-group">
                <textarea rows="5" name="description" placeholder="Event description" onChange={this.onChange} value={description} className={classnames('form-control', {'is-invalid': errors.description})} disabled={editForm}/>
                {errors.description && (<div className="invalid-feedback">{errors.description}</div>)}
              </div>

              <div className="form-row event-date-time">
                <div className="form-group col-md-6">
                  <label htmlFor="event-start-date" className="m-0">Event Start Date</label>
                  <Datetime
                      onChange={(start_date) => this.setState({start_date})}
                      value={start_date}
                      timeFormat={false}
                      className={classnames('form-control p-0 border-0', {'is-invalid': errors.start_date})}
                      dateFormat="DD-MM-YYYY"
                      inputProps={{ placeholder: 'DD-MM-YYYY', disabled: editForm }}
                  />
                  {errors.start_date && (<div className="invalid-feedback">{errors.start_date}</div>)}
                </div>

                <div className="form-group col-md-6">
                  <label htmlFor="event-start-time" className="m-0">Event Start Time</label>
                  <Datetime
                      onChange={(start_time) => this.setState({start_time})}
                      value={start_time}
                      dateFormat={false}
                      className={classnames('form-control p-0 border-0', {'is-invalid': errors.start_time})}
                      timeFormat="HH:mm"
                      inputProps={{ placeholder: '22:00', disabled: editForm }}
                  />
                  {errors.start_time && (<div className="invalid-feedback">{errors.start_time}</div>)}
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="event-end-date" className="m-0">Event End Date</label>
                  <Datetime
                      onChange={(end_date) => this.setState({end_date})}
                      value={end_date}
                      timeFormat={false}
                      className={classnames('form-control p-0 border-0', {'is-invalid': errors.end_date})}
                      dateFormat="DD-MM-YYYY"
                      inputProps={{ placeholder: 'DD-MM-YYYY', disabled: editForm }}
                  />
                  {errors.end_date && (<div className="invalid-feedback">{errors.end_date}</div>)}
                </div>

                <div className="form-group col-md-6">
                  <label htmlFor="event-end-time" className="m-0">Event End Time</label>
                  <Datetime
                      onChange={(end_time) => this.setState({end_time})}
                      value={end_time}
                      dateFormat={false}
                      className={classnames('form-control p-0 border-0', {'is-invalid': errors.end_time})}
                      timeFormat="HH:mm"
                      inputProps={{ placeholder: '06:00', disabled: editForm }}
                  />
                  {errors.end_time && (<div className="invalid-feedback">{errors.end_time}</div>)}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="event-source-url" className="m-0">Event Source</label>
                <input type="url" pattern="https://.*" name="source_url" placeholder="Event Source Url i.e. https://venue.de" onChange={this.onChange} value={source_url} className={classnames('form-control', {'is-invalid': errors.source_url})} disabled={editForm}/>
                {errors.source_url && (<div className="invalid-feedback">{errors.source_url}</div>)}
              </div>

              <label htmlFor="event-image" className="m-0">Event Image i.e. Flyer</label>
              <InputGroup
                  placeholder="Event Image URL"
                  name="media_library"
                  icon="far fa-image"
                  value={media_library}
                  onChange={this.onChange}
                  error={errors.media_library}
                  disabled={editForm}
              />

              <div className="form-group">
                <ReactTags
                    classNames={{
                      tagInputField: 'input-tags',
                    }}
                    id="event-tags-input"
                    tags={tagsObjectArray}
                    handleDelete={this.onDeleteEventTag}
                    handleAddition={this.onChangeEventTags}
                    delimiters={[KeyCodes.enter, KeyCodes.comma, KeyCodes.space, KeyCodes.minus, KeyCodes.hash]}
                    readOnly={editForm}
                />
              </div>

              {!editForm && <button className="btn btn-lg btn-success btn-block my-4" type="Submit">
                Save Event<i className="fas fa-lock pl-3"></i></button>}

              {editForm && <div className="btn btn-lg btn-danger btn-block my-4" onClick={() => {this.setState({editForm: !this.state.editForm})}}>
                Edit Form<i className="fas fa-lock-open pl-3"></i></div>}

            </form>
          </div>

        </div>
    );
  }
}

EventEditPage.propTypes = {
  errors: PropTypes.object.isRequired,
  getEvent: PropTypes.func.isRequired,
  editEvent: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  events: state.events,
  errors: state.errors,
});

export default connect(mapStateToProps, {getEvent, editEvent, clearErrors})(withRouter(EventEditPage));
