import React from 'react';
import Moment from 'react-moment';

const EventCardGigaTools = (props) => {
    const {name, venue, eventdate, event_owner, showtime, url, event_image} = props.event;

    return (
        <div className="card mb-2" id={name}>
            <h5 className="card-header event-header d-flex justify-content-between">
                <div className="event-name">{name}</div>
                <div className="control-buttons">
                    {url && <a href={url} className="event-source-url pl-2 text-success" aria-label="event source url" target="_blank" rel="noopener noreferrer"><i className="fas fa-globe"></i></a>}
                </div>
            </h5>
            <div className="card-body">
                <p className="event-venue"><i className="fas fa-map-marker-alt pr-2"></i>{venue}</p>
                {event_owner && <p className="event-owner"><i class="fas fa-user-cog pr-2"></i>{event_owner}</p>}
                {eventdate && <p className="event-date"><i className="fas fa-calendar-alt pr-2"></i>{eventdate}</p>}
                {showtime && <p className="event-time"><i className="fas fa-clock pr-2"></i><Moment format="HH:mm">{showtime}</Moment></p>}
                {event_image && <img className="event-image" src={event_image} style={{maxWidth: '100px'}} />}
            </div>
        </div>
    );
};

export default EventCardGigaTools;
