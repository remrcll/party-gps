import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {logoutUser} from "../../actions/authActions";
import {withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import userIcon from '../../assets/default_user.png';
import logo from '../../assets/logo.svg';

class Navbar extends Component {
  onLogout = e => {
    e.preventDefault();
    this.props.logoutUser();
  }

  render() {
    const {user, isAuthenticated} = this.props.auth;
    let avatar = '';
    if(isAuthenticated) {
      (user.avatar !== '') ? avatar = user.avatar : avatar = userIcon;
    }

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img src={logo} width="30" height="30" alt="partygps logo" />
          </Link>

          {isAuthenticated && (
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                  aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          )}

          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            {/*left menu*/}
            {isAuthenticated && (
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="/dashboard">Dashboard</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/venues">Venues</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/events">Events</Link>
              </li>
            </ul>
            )}

            {/* right menu */}
            {isAuthenticated && (
            <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img className="rounded-circle" src={avatar} alt='user profile' style={{width: '25px', height: '25px', marginRight: '5px'}} title="user avatar!"/>
                </a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <span className="dropdown-item pb-0 user-name disable-pointer"><strong>@{user.name}</strong></span>
                  <span className="dropdown-item pt-0 text-muted user-username disable-pointer small">{user.role}</span>
                  {(user.role !== 'guest') && <div className="dropdown-divider"></div>}
                  {(user.role !== 'guest') && <Link to="/venues/add" className="dropdown-item"><i className="fas fa-building pr-3"></i>Add Venue</Link>}
                  {(user.role !== 'guest') && <Link to="/events/add" className="dropdown-item"><i className="fas fa-calendar-check pr-3"></i>Add Events</Link>}
                  {(user.role === 'admin' || (user.role === 'moderator')) && <Link to="/events/import" className="dropdown-item"><i className="fas fa-file-code pr-3"></i>Import Events</Link>}
                  <div className="dropdown-divider"></div>
                  <Link className="dropdown-item" to="" onClick={this.onLogout}>
                    <span className="fa fa-power-off pr-3"></span>Logout
                  </Link>
                </div>
              </li>
            </ul>
            )}
          </div>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {logoutUser})(withRouter(Navbar));
