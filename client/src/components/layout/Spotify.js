import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
// import SpotifyWebApi from 'spotify-web-api-js';
import getHashParams from '../../utils/getHashParams';
import setAuthToken from '../../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import {setCurrentUser} from "../../actions/authActions";

// const spotifyApi = new SpotifyWebApi();

class Spotify extends Component {
  constructor(){
    super();
    this.state = {
      loggedIn: false,
    }
  }

  componentDidMount() {
    const params = getHashParams();
    const token = params.token;
    const error = params.error;
    if (error) {
      alert('There was an error during the authentication');
    } else {
      if (token) {
        // set token to local storage
        localStorage.setItem('jwtToken', token);
        // set token to auth header
        setAuthToken(token);
        // decode token to get user data
        const decodedData = jwt_decode(token);
        setCurrentUser(decodedData);
        window.location.href = '/login';
        console.log(decodedData);
        // const configOptions = {
        //   url: 'https://cors-anywhere.herokuapp.com/http://api.spotify.com/v1/me',
        //   method: 'post',
        //   headers: {
        //     'Content-Type': 'application/json',
        //     'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        //     'Access-Control-Allow-Origin': '*',
        //     'Authorization': 'Bearer ' + access_token
        //   },
        // }

        // axios(configOptions)
        // .then(response => {
        //   if(response.status === 200) {
        //     const {display_name, email, images} = response.data;
        //     const payload = {
        //       id: display_name,
        //       user_id: display_name,
        //       name: display_name,
        //       email: email,
        //       phone: '',
        //       avatar: images[0],
        //       role: 'guest',
        //     }
        //     console.log(payload);
        //   }
        // })
        // .catch(err => {
        //   console.log(err);
        // })
      }
    }
  }


  render() {
    return (
        <div></div>
    );
  }
}

Spotify.propTypes = {
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {})(withRouter(Spotify));
