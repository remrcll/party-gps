import React from 'react';
import { Link } from 'react-router-dom';

const OverviewPanel = (props) => {
  return (
    <div className="card bg-dark text-white mt-3" id="overview-panel">
      <div className="card-header">
        Party GPS
      </div>
      <div className="card-body text-center d-flex flex-wrap">
        <div className="col-md-3 p-2">
          <Link to="/venues" className="link text-white">
            <div className="bg-danger p-3 rounded">
              <h1>{props.venues}</h1>
              <h1><i className="fas fa-building"></i></h1>
              <h4>Venues</h4>
            </div>
          </Link>
        </div>
        <div className="col-md-3 p-2">
          <Link to="/events" className="link text-white">
            <div className="bg-danger p-3 rounded">
              <h1>{props.events}</h1>
              <h1><i className="fas fa-calendar-check"></i></h1>
              <h4>Events</h4>
            </div>
          </Link>
        </div>
        <div className="col-md-3 p-2">
          <div className="bg-danger p-3 rounded">
            <h1>{props.users}</h1>
            <h1><i className="fas fa-users"></i></h1>
            <h4>Users</h4>
          </div>
        </div>
        <div className="col-md-3 p-2">
          <div className="bg-danger p-3 rounded">
            <h1>0</h1>
            <h1><i className="fas fa-random"></i></h1>
            <h4>Random</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OverviewPanel;
