import React, {Component} from 'react';
import Navbar from "./Navbar";
import connect from "react-redux/es/connect/connect";
import PropTypes from 'prop-types';
import {withRouter} from "react-router-dom";
import {getVenues} from "../../actions/venueActions";
import {getEvents} from "../../actions/eventActions";
import {getUsers} from "../../actions/userActions";
import {getUserFollowedArtists} from "../../actions/authActions";
import OverviewPanel from '../layout/OverviewPanel';
import Users from '../users/Users';
import SpotifyWebApi from 'spotify-web-api-js';
const spotifyApi = new SpotifyWebApi();

class Dashboard extends Component {
  componentDidMount() {
    const {user} = this.props.auth;
    this.props.getVenues();
    this.props.getEvents();
    this.props.getUsers();

    // get spotify loggedin user data
    spotifyApi.setAccessToken(user.access_token);

    // get user 50 recently played tracks
    spotifyApi.getMyRecentlyPlayedTracks({limit: 50})
        .then(recentTracks => {

          // create array of 50 recently played tracks Artist ID's
          let latestPlayedTracksArtistID = [];
          const userRecentTracks = recentTracks.items;
          userRecentTracks.map((each, key) => {
            latestPlayedTracksArtistID.push(each.track.artists[0].id);
          })

          // get artists data using 'latestPlayedTracksArtistID'
          spotifyApi.getArtists(latestPlayedTracksArtistID)
              .then(artistsData => {
                console.log(artistsData);
                // create array of genres
                // genres[] = User 50 recently played tracks => artists => genres's
                let genres = [];
                const {artists} = artistsData;
                artists.map((each, key) => {
                  genres.push(...each.genres);
                })

                genres = genres.filter(function (gen, index) {
                  return genres.indexOf(gen) === index;
                });
                // console.log('User Latest 50 played Tracks artist Genres: ', genres);
              });
        })
        .catch(err => {
          console.log(err);
        });
  }

  render() {
    const {name, role} = this.props.auth.user;
    const {venues} = this.props.venues;
    const {events} = this.props.events;
    const {users} = this.props.users;
    let venuesCount = 0, eventsCount = 0, usersCount = 0;

    if(events && venues && users) {
      venuesCount= venues.length;
      eventsCount= events.length;
      usersCount= users.length;
    }

    const currentUserDataHtml = (
      <div className="current-user-data mt-3">
        <h4>Welcome <span className="text-danger">{name}</span>!</h4>
      </div>
    )

    const usersViewHtml = (
      <div className="users-table mt-3">
        <Users currentUser={this.props.auth.user}/>
      </div>
    );

    return (
      <div id="dashboard">
        <Navbar/>
        <div className="container">
          {currentUserDataHtml}

          <OverviewPanel venues={venuesCount} events={eventsCount} users={usersCount}/>

          {(role === 'admin') ? usersViewHtml : null}
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  auth: PropTypes.object.isRequired,
  venues: PropTypes.object.isRequired,
  events: PropTypes.object.isRequired,
  users: PropTypes.object.isRequired,
  getEvents: PropTypes.func.isRequired,
  getVenues: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  venues: state.venues,
  events: state.events,
  users: state.users,
});

export default connect(mapStateToProps, {getVenues, getEvents, getUsers})(withRouter(Dashboard));
