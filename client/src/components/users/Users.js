import React, {Component} from 'react';
import classnames from "classnames";
import connect from "react-redux/es/connect/connect";
import PropTypes from 'prop-types';
import {withRouter} from "react-router-dom";
import {getUsers, deleteUser} from "../../actions/userActions";
import Loading from "../common/Loading";
import {AlertList} from "react-bs-notifier";
import {clearErrors} from "../../actions/errorActions";

class Users extends Component {
  state = {
    alerts: [],
  }

  componentDidMount() {
    this.props.getUsers();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors.alert) {
      this.setState({
        alerts: [nextProps.errors.alert]
      });
    }
  }

  deleteUser = (user_id) => {
    this.props.deleteUser(user_id);
  }

  render() {
    const {users, loading} = this.props.users;
    const {currentUser} = this.props;
    const {alerts} = this.state;
    let html;

    if(users && !loading) {
      html = users.map((user, index) =>
        (
          <tr key={index+1} className={classnames('', {'bg-danger': (user.role === 'block')})}>
            <th scope="row">{index+1}</th>
            <td className="align-middle">{user.name}{(user.name === currentUser.name) && (<span className="badge badge-danger ml-2">you</span>)}</td>
            <td className="align-middle">{user.email}</td>
            <td className="align-middle">{user.role}</td>
            {/*<td className="align-middle"><button type="submit" className="btn">Edit</button></td>*/}
            <td className="align-middle"><i className="fas fa-trash-alt" onClick={this.deleteUser.bind(this, user.user_id)}></i></td>
          </tr>
        )
      );
    } else {
      html =
        (<tr>
          <td><Loading/></td>
         </tr>)
    }

    return (
      <div id="users">
        {alerts &&
        (
          <div className="alert-wrapper">
            <AlertList
              position="top-right"
              alerts={alerts}
              timeout={3000}
              onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
            />
          </div>
        )
        }

        <div className="card bg-dark text-white mt-4" id="overview-panel">
          <div className="card-header">
            Users Overview
          </div>
          <div className="card-body m-0 p-0">
            <table className="table table-dark table-striped users-table text-center mb-0">
              <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                {/*<th scope="col">Update</th>*/}
                <th scope="col">Remove</th>
              </tr>
              </thead>
              <tbody>
              {html}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

Users.propTypes = {
  users: PropTypes.object.isRequired,
  getUsers: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  users: state.users,
  errors: state.errors
});

export default connect(mapStateToProps, {getUsers, deleteUser, clearErrors})(withRouter(Users));