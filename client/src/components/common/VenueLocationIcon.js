import L from 'leaflet';

export const VenueLocationIcon = L.icon({
  iconUrl: require('../../assets/venue_location_icon.png'),
  iconRetinaUrl: require('../../assets/venue_location_icon.png'),
  iconAnchor: null,
  // popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [35, 35],
  className: 'leaflet-div-icon'
});

/*
* other icon gif urls
*
* http://www.clker.com/cliparts/W/x/V/N/a/o/orange-pin.svg
*
* */