import L from 'leaflet';

export const EventLocationIcon = L.icon({
  iconUrl: require('../../assets/event_location_icon.png'),
  iconRetinaUrl: require('../../assets/event_location_icon.png'),
  iconAnchor: null,
  // popupAnchor: null,
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  iconSize: [30, 30],
  className: 'leaflet-div-icon'
});

/*
* other icon gif urls
*
* selected: https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/party-popper_1f389.png
* https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/party-popper_1f389.png
* https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Emoji_u1f389.svg/240px-Emoji_u1f389.svg.png
* http://icons.iconarchive.com/icons/google/noto-emoji-activities/1024/52707-party-popper-icon.png
*
* */