import React from 'react';
import { Link } from 'react-router-dom';
import isEmpty from '../../utils/isEmpty';

class VenueCard extends React.Component {
  render() {
    const {venue_id, category, name, display, address: {line1, line2, postal, city, country}, phone, email, website, tags, social_media} = this.props.venue;
    const {role} = this.props;
    return (
      <div className="card mb-2" id={venue_id}>
        <h5 className="card-header venue-header d-flex justify-content-between">
          <div className="venue-name"><Link to={`/venue/${venue_id}`}>{name}</Link></div>
          <div className="venue-display">{display}</div>
          <div className="control-buttons">
            {website && <a href={website} className="venue-website pl-2 text-success" aria-label="venue website" target="_blank" rel="noopener noreferrer"><i className="fas fa-globe website"></i></a>}
            {(role === 'admin' || role === 'moderator') && <Link to={`/venue/edit/${venue_id}`}><i className="far fa-edit pl-2 edit"></i></Link>}
            {role === 'admin' && <i className="fas fa-trash-alt pl-2 delete" id={venue_id} onClick={this.props.delete}></i>}
          </div>
        </h5>
        <div className="card-body">
          <p className="venue-address">
            <i className="fas fa-map-marker-alt pr-2"></i>
            {line1 && line1+', '}
            {line2 && line2+', '}
            {postal && postal+', '}
            {city} {country}
          </p>
          {phone && <p className="venue-phone"><i className="fas fa-phone-square pr-2"></i>{phone}</p>}
          {email && <p className="venue-email"><i className="fas fa-envelope pr-2"></i>{email}</p>}
          {(category || tags) && (
              <div className="venue-category" id="display-category">
                {category && (category.map((eachCategory, index) => (<span key={index} className='category-wrapper category-bg'>{eachCategory}</span>)))}
                {tags && (tags.map((tag, index) => (<span key={index} className='tag-wrapper tag-bg'>{tag}</span>)))}
              </div>
          )}
          <div className="d-flex pt-2 justify-content-end venue-social-media">
            {isEmpty(social_media && social_media.facebook) ? null : (
                <a className="text-success pl-2" href={social_media.facebook} target="_blank" without rel="noopener noreferrer">
                  <i className="fab fa-facebook facebook"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.instagram) ? null : (
                <a className="text-dark pl-2" href={social_media.instagram} target="_blank" without rel="noopener noreferrer">
                  <i className="fab fa-instagram instagram"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.twitter) ? null : (
                <a className="text-info pl-2" href={social_media.twitter} target="_blank" without rel="noopener noreferrer">
                  <i className="fab fa-twitter twitter"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.youtube) ? null : (
                <a className="text-danger pl-2" href={social_media.youtube} target="_blank" without rel="noopener noreferrer">
                  <i className="fab fa-youtube youtube"></i>
                </a>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default VenueCard;
