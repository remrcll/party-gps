import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {getVenue, editVenue} from "../../actions/venueActions";
import isEmpty from '../../utils/isEmpty';
import Select from 'react-select';
import { WithContext as ReactTags } from 'react-tag-input';
import PhoneInput, {isValidPhoneNumber} from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import classnames from "classnames";
import {AlertList} from "react-bs-notifier";
import {clearErrors} from "../../actions/errorActions";
import Navbar from "../layout/Navbar";
import InputGroup from "../common/InputGroup";
import defaultVenueImage from '../../assets/sidebar_header_venue.jpg';

const defaultVenueCategories = [
  { value: 'Bar', label: 'Bar' },
  { value: 'Cafe', label: 'Cafe' },
  { value: 'Club', label: 'Club' },
  { value: 'Festival', label: 'Festival' },
  { value: 'Open-air', label: 'Open-air' },
  { value: 'Parade', label: 'Parade' },
  { value: 'Other', label: 'Other' },
];

const KeyCodes = {
  comma: 188,
  enter: 13,
  space: 32,
  minus: 189,
  hash: 191
};

class VenueEditPage extends Component {
  state = {
    venue_id: '',
    name: '',
    display: '',
    line1: '',
    line2: '',
    postal: '',
    city: 'Berlin',
    country: 'Germany',
    geometry: '',
    phone: '',
    email: '',
    website: '',
    description: '',
    avatar: '',
    category: [],
    tags: [],
    facebook: '',
    instagram: '',
    twitter: '',
    youtube: '',
    errors: {},
    editForm: true,
    alerts: [],
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const {venue_id, name, display, line1, line2, postal, city, country, geometry, phone, email, website, description, avatar, category, tags, facebook, instagram, twitter, youtube} = this.state;

    const userInput = {
      venue_id,
      name,
      display,
      line1,
      line2,
      postal,
      city,
      country,
      geometry,
      phone,
      email,
      website,
      description,
      avatar,
      category,
      tags,
      facebook,
      instagram,
      twitter,
      youtube,
    }

    this.setState({editForm: true});

    this.props.editVenue(userInput, this.props.history);
  }

  onChangeVenueCategory = (category) => {
    let categories = []
    category.forEach((category) => categories.push(category.value));
    this.setState({ category: categories });
  }

  onDeleteVenueTag = (i) => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i),
    });
  }

  onChangeVenueTags = (tag) => {
    this.setState(state => ({ tags: [...state.tags, '#'+tag.id.toLowerCase()] }));
  }

  componentDidMount() {
    const venue_id = this.props.match.params.venue_id;
    this.props.getVenue(venue_id);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.venues.venue === null && this.props.venues.loading) {
      this.props.history.push('/pagenotfound');
    }

    if(nextProps.errors.alert) {
      this.setState({
        alerts: [nextProps.errors.alert]
      });
    }

    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }

    if(nextProps.venues.venue) {
      // storing fetched venue data in a const venue
      const venue = nextProps.venues.venue;

      // make fields that are not filled, equal to empty strings
      venue.venue_id = !isEmpty(venue.venue_id) ? venue.venue_id : '';
      venue.name = !isEmpty(venue.name) ? venue.name : '';
      venue.display = !isEmpty(venue.display) ? venue.display : '';
      venue.line1 = !isEmpty(venue.address.line1) ? venue.address.line1 : '';
      venue.line2 = !isEmpty(venue.address.line2) ? venue.address.line2 : '';
      venue.postal = !isEmpty(venue.address.postal) ? venue.address.postal.toString() : '';
      venue.city = !isEmpty(venue.address.city) ? venue.address.city : '';
      venue.country = !isEmpty(venue.address.country) ? venue.address.country : '';
      venue.geometry = !isEmpty(venue.geometry) ? venue.geometry.toString() : [];
      venue.phone = !isEmpty(venue.phone) ? venue.phone : '';
      venue.email = !isEmpty(venue.email) ? venue.email : '';
      venue.website = !isEmpty(venue.website) ? venue.website : '';
      venue.description = !isEmpty(venue.description) ? venue.description : '';
      venue.avatar = !isEmpty(venue.avatar) ? venue.avatar : defaultVenueImage;
      venue.category = !isEmpty(venue.category) ? venue.category : [];
      venue.tags = !isEmpty(venue.tags) ? venue.tags : [];
      venue.facebook = !isEmpty(venue.social_media.facebook) ? venue.social_media.facebook : '';
      venue.instagram = !isEmpty(venue.social_media.instagram) ? venue.social_media.instagram : '';
      venue.twitter = !isEmpty(venue.social_media.twitter) ? venue.social_media.twitter : '';
      venue.youtube = !isEmpty(venue.social_media.youtube) ? venue.social_media.youtube : '';

      // set all fields to the state
      this.setState({
        venue_id: venue.venue_id,
        name: venue.name,
        display: venue.display,
        line1: venue.line1,
        line2: venue.line2,
        postal: venue.postal,
        city: 'Berlin',
        country: 'Germany',
        geometry: venue.geometry,
        phone: venue.phone,
        email: venue.email,
        website: venue.website,
        description: venue.description,
        avatar: venue.avatar,
        category: venue.category,
        tags: venue.tags,
        facebook: venue.facebook,
        instagram: venue.instagram,
        twitter: venue.twitter,
        youtube: venue.youtube,
      });

    }
  }

  render() {
    const {venue_id, name, display, line1, line2, postal, city, country, geometry, phone, email, website, description, avatar, category, tags, facebook, instagram, twitter, youtube, errors, editForm, alerts} = this.state;

    // for react-select create an array of objects, in which each object contains key value pairs of 'value' & 'label'
    let categoriesObjectArray = [];
    if(category) {
      category.forEach((eachCategory) => {
        let eachCategoryObject = {}
        eachCategoryObject['value'] = eachCategory;
        eachCategoryObject['label'] = eachCategory;
        categoriesObjectArray.push(eachCategoryObject);
      })
    }

    let tagsObjectArray = [];
    if(tags) {
      tags.forEach((eachTag) => {
        let eachTagObject = {}
        eachTagObject['id'] = eachTag;
        eachTagObject['text'] = eachTag;
        tagsObjectArray.push(eachTagObject);
      })
    }

    const show = (editForm) ? ('none') : ('block');
    const socialInputs = (
        <div style={{'display': show}}>
          <InputGroup
              placeholder="Facebook"
              name="facebook"
              icon="fab fa-facebook facebook"
              value={facebook}
              onChange={this.onChange}
              error={errors.facebook}
          />
          <InputGroup
              placeholder="Instagram"
              name="instagram"
              icon="fab fa-instagram instagram"
              value={instagram}
              onChange={this.onChange}
              error={errors.instagram}
          />
          <InputGroup
              placeholder="Twitter"
              name="twitter"
              icon="fab fa-twitter twitter"
              value={twitter}
              onChange={this.onChange}
              error={errors.twitter}
          />
          <InputGroup
              placeholder="Youtube"
              name="youtube"
              icon="fab fa-youtube youtube"
              value={youtube}
              onChange={this.onChange}
              error={errors.youtube}
          />
        </div>
    );

    return (
        <div id="venue-edit-page">
          {alerts &&
          (
              <div className="alert-wrapper">
                <AlertList
                    position="top-right"
                    alerts={alerts}
                    timeout={3000}
                    onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
                />
              </div>
          )
          }

          <Navbar/>

          <div className="container p-0">
            <form name="edit-venue-form" className="form-style m-auto" noValidate onSubmit={this.onSubmit} autoComplete="off">
              <h3 className="text-center">Edit Venue</h3>
              <hr className="colorgraph mb-5" />

              <div className="form-group">
                <div className="form-control bg-dark text-white">VenueID: {venue_id}</div>
              </div>

              <div className="form-group">
                <div className="form-control bg-dark text-white">{name}</div>
              </div>

              <div className="form-group">
                <input type="text" name="display" placeholder="Display Name (for gigatool)" onChange={this.onChange} value={display} className={classnames('form-control', {'is-invalid': errors.display})} disabled={editForm}/>
                {errors.display && (<div className="invalid-feedback">{errors.display}</div>)}
              </div>

              <Select
                  isMulti
                  placeholder="Venue Category"
                  name="category"
                  options={defaultVenueCategories}
                  classNamePrefix="select"
                  onChange={this.onChangeVenueCategory}
                  value={categoriesObjectArray}
                  className={classnames('basic-multi-select form-control border-0 p-0 pb-3', {'is-invalid': errors.category})}
                  isDisabled={editForm}
              />
              {errors.category && (<div className="invalid-feedback pb-3">{errors.category}</div>)}

              <div className="form-row">
                <div className="form-group col-md-12">
                  <input type="text" name="line1" placeholder="Line1: 1234 Main Str." onChange={this.onChange} value={line1} className={classnames('form-control', {'is-invalid': errors.line1})}  disabled={editForm}/>
                  {errors.line1 && (<div className="invalid-feedback">{errors.line1}</div>)}
                </div>
                <div className="form-group col-md-12">
                  <input type="text" name="line2" placeholder="Line2: Apartment, studio, or floor" onChange={this.onChange} value={line2} className={classnames('form-control', {'is-invalid': errors.line2})}  disabled={editForm}/>
                  {errors.line2 && (<div className="invalid-feedback">{errors.line2}</div>)}
                </div>

                <div className="form-group col-md-4">
                  <input type="text" name="postal" placeholder="Postal" onChange={this.onChange} value={postal} className={classnames('form-control', {'is-invalid': errors.postal})} disabled={editForm}/>
                  {errors.postal && (<div className="invalid-feedback">{errors.postal}</div>)}
                </div>
                <div className="form-group col-md-8">
                  <input disabled="disabled" type="text" name="city" placeholder="City" onChange={this.onChange} value={city} className={classnames('form-control', {'is-invalid': errors.city})}/>
                  {errors.city && (<div className="invalid-feedback">{errors.city}</div>)}
                </div>
                <div className="form-group col-md-12">
                  <input disabled="disabled" type="text" name="country" placeholder="Country" onChange={this.onChange} value={country} className={classnames('form-control', {'is-invalid': errors.country})}/>
                  {errors.country && (<div className="invalid-feedback">{errors.country}</div>)}
                </div>
              </div>

              <div className="form-group">
                <input type="text" name="geometry" placeholder="Latitude,Longitude i.e. 52.516399,13.377736" onChange={this.onChange} value={geometry} className={classnames('form-control', {'is-invalid': errors.geometry})} disabled={editForm}/>
                {errors.geometry && (<div className="invalid-feedback">{errors.geometry}</div>)}
              </div>

              <div className="form-group">
                <PhoneInput
                    placeholder="Phone"
                    value={phone}
                    onChange={phone => this.setState({ phone })}
                    country="DE"
                    error={ phone ? (isValidPhoneNumber(phone) ? undefined : 'Invalid phone number') : '' }
                    disabled={editForm}
                />
              </div>

              <div className="form-group">
                <input type="email" name="email" placeholder="Email" onChange={this.onChange} value={email} className={classnames('form-control', {'is-invalid': errors.email})} disabled={editForm}/>
                {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
              </div>

              <div className="form-group">
                <input type="url" pattern="https://.*" name="website" placeholder="Website i.e. https://venue.de" onChange={this.onChange} value={website} className={classnames('form-control', {'is-invalid': errors.website})} disabled={editForm}/>
                {errors.website && (<div className="invalid-feedback">{errors.website}</div>)}
              </div>

              <div className="form-group">
                <textarea rows="5" name="description" placeholder="Description" onChange={this.onChange} value={description} className={classnames('form-control', {'is-invalid': errors.description})} disabled={editForm}/>
                {errors.description && (<div className="invalid-feedback">{errors.description}</div>)}
              </div>

              <InputGroup
                  placeholder="Venue Image URL"
                  name="avatar"
                  icon="far fa-image"
                  value={avatar}
                  onChange={this.onChange}
                  error={errors.avatar}
              />

              <div className="form-group">
                <ReactTags
                  classNames={{
                    tagInputField: 'input-tags',
                  }}
                  id="venue-tags-input"
                  tags={tagsObjectArray}
                  handleDelete={this.onDeleteVenueTag}
                  handleAddition={this.onChangeVenueTags}
                  delimiters={[KeyCodes.enter, KeyCodes.comma, KeyCodes.space, KeyCodes.minus, KeyCodes.hash]}
                  readOnly={editForm}
                />
              </div>

              {socialInputs}

              {!editForm && <button className="btn btn-lg btn-success btn-block my-4" type="Submit">
                Save Venue<i className="fas fa-lock pl-3"></i></button>}

              {editForm && <div className="btn btn-lg btn-danger btn-block my-4" onClick={() => {this.setState({editForm: !this.state.editForm})}}>
                Edit Form<i className="fas fa-lock-open pl-3"></i></div>}
            </form>
          </div>
        </div>
    );
  }
}

VenueEditPage.propTypes = {
  venues: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  getVenue: PropTypes.func.isRequired,
  editVenue: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  venues: state.venues,
  errors: state.errors,
});

export default connect(mapStateToProps, {getVenue, editVenue, clearErrors})(withRouter(VenueEditPage));
