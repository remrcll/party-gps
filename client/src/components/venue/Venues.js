import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link, withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import Loading from "../common/Loading";
import VenueCard from "./VenueCard";
import {getVenues, deleteVenue} from "../../actions/venueActions";
import {AlertList} from "react-bs-notifier";
import {clearErrors} from "../../actions/errorActions";
import Navbar from "../layout/Navbar";
import Select from 'react-select';

const defaultFilterBy = [
  { value: 'category', label: 'Category' },
  { value: 'name', label: 'Name' },
  { value: 'tags', label: 'Tags' },
];

class Venues extends Component {
  state = {
    alerts: [],
    keyword: '',
    filter_by: [],
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors.alert) {
      this.setState({
        alerts: [nextProps.errors.alert]
      });
    }
  }

  componentDidMount() {
    this.props.getVenues();
  }

  deleteVenue = (venue_id) => {
    this.props.deleteVenue(venue_id);
  }

  render() {
    const {venues, loading} = this.props.venues;
    const {role} = this.props.auth.user;
    const {alerts, keyword, filter_by} = this.state;
    let selectedFilters = (filter_by.length > 0) ? filter_by.map(filter => filter.value) : '';
    let html, totalVenuesCount, filteredVenues;

    // console.log('Keyword: ', keyword);
    // console.log('FilterBy: ', filter_by);
    // console.log('SelectedFilters: ', selectedFilters);

    if(venues && !loading) {
      // lets start filtering venues
      let category, name, tags, results;

      filteredVenues = venues.filter((venue) => {

        // function below check in each venue category list (i.e. club, bar, cafe) that given input exist or not, and return an array
        let filterCategory = (input) => venue.category.filter(eachCategory => eachCategory.toLowerCase().indexOf(input.toLowerCase()) > -1)
        let filterTags = (input) => venue.tags.filter(eachTag => eachTag.toLowerCase().indexOf(input.toLowerCase()) > -1)

        if(selectedFilters.length !== 0) {
          // if user selected fields filters
          category = (filterCategory(keyword.toLowerCase()).length !== 0) && (selectedFilters.indexOf('category') !== -1);
          name = (venue.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) && (selectedFilters.indexOf('name') !== -1);
          tags = (filterTags(keyword.toLowerCase()).length !== 0) && (selectedFilters.indexOf('tags') !== -1);
        } else {
          // search all
          category = filterCategory(keyword.toLowerCase()).length !== 0;
          name = venue.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
          tags = filterTags(keyword.toLowerCase()).length !== 0;
        }

        results = (category || name || tags);

        return results;
      });

      filteredVenues.length === 0 ? totalVenuesCount = 'No venue record found.' : totalVenuesCount = filteredVenues.length;
      html = (filteredVenues).map((venue, index) =>
        (<VenueCard key={index} venue={venue} role={role} delete={this.deleteVenue.bind(this, venue.venue_id)}/>)
      );

    } else {
      html = <Loading/>;
    }

    return(
        <div id="venues">
          {alerts &&
          (
              <div className="alert-wrapper">
                <AlertList
                    position="top-right"
                    alerts={alerts}
                    timeout={3000}
                    onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
                />
              </div>
          )
          }

          <Navbar/>

          <div className="container">
            {/*venues filter*/}
            <div className="card bg-dark text-white my-3">
              <div className="card-header">
                {/*venues count block*/}
                <div className="venues-header d-flex justify-content-between align-items-center">
                  <div className="lead">
                    Venues:{totalVenuesCount}
                  </div>
                  <Link to="/venues/add"><i className="fas fa-plus px-2"></i>Venue</Link>
                </div>
              </div>
              <div className="card-body">
                <p className="card-text">This is an auto submit form, you can filter venues more specifically by choosing field filters.</p>
                {/*filter form*/}
                <form name="filter-venues-form" className="m-auto" noValidate autoComplete="off">
                  <div className="form-row">
                    <div className="form-group col-md-6">
                      <input
                          type="text"
                          name="keyword"
                          placeholder="Keyword..."
                          onChange={(e) => this.setState({keyword: e.target.value})}
                          value={keyword}
                          className="form-control"/>
                    </div>

                    <div className="form-group col-md-6">
                      <Select
                          isMulti
                          placeholder="Filter by venue..."
                          name="filter_by"
                          classNamePrefix="select"
                          options={defaultFilterBy}
                          onChange={(filter_by) => this.setState({filter_by})}
                          className="basic-multi-select form-control border-0 p-0"
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>

            {/*venues card*/}
            <div id="clubs-cards">
              {html}
            </div>
          </div>
        </div>
    );
  }
}

Venues.propTypes = {
  getVenues: PropTypes.func.isRequired,
  deleteVenue: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
  venues: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  venues: state.venues,
  errors: state.errors,
});

export default connect(mapStateToProps, {getVenues, deleteVenue, clearErrors})(withRouter(Venues));
