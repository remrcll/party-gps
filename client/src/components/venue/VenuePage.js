import React, {Component} from 'react';
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import { Link } from 'react-router-dom';
import {withRouter} from "react-router-dom";
import {getVenue} from "../../actions/venueActions";
import Loading from "../common/Loading";
import Navbar from "../layout/Navbar";
import isEmpty from "../../utils/isEmpty";

class VenuePage extends Component {
  componentDidMount() {
    const venue_id = this.props.match.params.venue_id;
    this.props.getVenue(venue_id);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.venues.venue === null && this.props.venues.loading) {
      this.props.history.push('/pagenotfound');
    }
  }

  render() {
    const {venue, loading} = this.props.venues;
    const {isAuthenticated, user: {role}} = this.props.auth;

    let html;
    if(venue && !loading) {
      const {venue_id, category, name, display, address: {line1, line2, postal, city, country}, phone, email, website, description, tags, social_media} = venue;

      html = (
        <div>
          {(isAuthenticated)
              ?
                (<Link to='/venues'><i className="fas fa-arrow-circle-left pr-2 py-3"></i>Venues</Link>)
                  :
                (<Link to='/'><i className="fas fa-arrow-circle-left pr-2 py-3"></i>Go Back</Link>)
          }

          <h1 className="">{venue_id}: {name}</h1>
          {display && <p className="venue-display">{display}</p>}
          <p className="venue-address">
            <i className="fas fa-map-marker-alt pr-2"></i>
            {line1 && line1+', '}
            {line2 && line2+', '}
            {postal && postal+', '}
            {city} {country}
          </p>
          {phone && <p className="venue-phone"><i className="fas fa-phone-square pr-2"></i>{phone}</p>}
          {email && <p className="venue-email"><i className="fas fa-envelope pr-2"></i>{email}</p>}
          {description && <p className="venue-description">{description}</p>}
          {(category || tags) &&
          (<div className="category-tags-container mb-3">
            <div className="venue-category" id="display-category">
              {category && (category.map((eachCategory, index) => (<span key={index} className='category-wrapper category-bg'>{eachCategory}</span>)))}
            </div>
            <div className="venue-tags" id="display-tags">
              {tags && (tags.map((tag, index) => (<span key={index} className='tag-wrapper tag-bg'>{tag}</span>)))}
            </div>
          </div>)}
          <div className="d-flex pt-2 venue-social-media">
            {(role === 'admin' || role === 'moderator') && <Link to={`/venue/edit/${venue_id}`}><i className="far fa-edit text-info pr-3" style={{'fontSize': '30px'}}></i></Link>}

            {isEmpty(social_media && social_media.facebook) ? null : (
                <a className="text-success pr-3" href={social_media.facebook} target="_blank" rel="noopener noreferrer">
                  <i className="fab fa-facebook fa-2x facebook"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.instagram) ? null : (
                <a className="text-dark pr-3" href={social_media.instagram} target="_blank" rel="noopener noreferrer">
                  <i className="fab fa-instagram fa-2x instagram"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.twitter) ? null : (
                <a className="text-info pr-3" href={social_media.twitter} target="_blank" rel="noopener noreferrer">
                  <i className="fab fa-twitter fa-2x twitter"></i>
                </a>
            )}

            {isEmpty(social_media && social_media.youtube) ? null : (
                <a className="text-danger pr-3" href={social_media.youtube} target="_blank" rel="noopener noreferrer">
                  <i className="fab fa-youtube fa-2x youtube"></i>
                </a>
            )}

            {isEmpty(website) ? null : (
                <a className="pr-3" href={website} target="_blank" rel="noopener noreferrer">
                  <i className="fas fa-globe website" style={{'fontSize': '30px'}}></i>
                </a>
            )}
          </div>
        </div>
      )
    } else {
      html = <Loading/>;
    }

    return (
      <div id="venue-page">
        <Navbar/>

        <div className="container">
          {html}
        </div>
      </div>
    );
  }
}

VenuePage.propTypes = {
  auth: PropTypes.object.isRequired,
  venues: PropTypes.object.isRequired,
  getVenue: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  venues: state.venues,
});

export default connect(mapStateToProps, {getVenue})(withRouter(VenuePage));
