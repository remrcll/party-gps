import React, {Component} from 'react';
import classnames from "classnames";
import PropTypes from 'prop-types';
import connect from "react-redux/es/connect/connect";
import {withRouter} from "react-router-dom";
import {addVenue} from "../../actions/venueActions";
import Select from 'react-select';
import { WithContext as ReactTags } from 'react-tag-input';
import PhoneInput, {isValidPhoneNumber} from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import Navbar from "../layout/Navbar";
import InputGroup from '../common/InputGroup';

const defaultVenueCategories = [
  { value: 'Bar', label: 'Bar' },
  { value: 'Cafe', label: 'Cafe' },
  { value: 'Club', label: 'Club' },
  { value: 'Festival', label: 'Festival' },
  { value: 'Open-air', label: 'Open-air' },
  { value: 'Parade', label: 'Parade' },
  { value: 'Other', label: 'Other' },
];

const KeyCodes = {
  comma: 188,
  enter: 13,
  space: 32,
  minus: 189,
  hash: 191
};

class AddVenue extends Component {
  state = {
    name: '',
    display: '',
    line1: '',
    line2: '',
    postal: '',
    city: 'Berlin',
    country: 'Germany',
    geometry: '',
    phone: '',
    email: '',
    website: '',
    description: '',
    category: [],
    tags: [],
    facebook: '',
    instagram: '',
    twitter: '',
    youtube: '',
    displaySocialInput: false,
    errors: {},
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const {name, display, line1, line2, postal, city, country, geometry, phone, email, website, description, category, tags, facebook, instagram, twitter, youtube} = this.state;

    const userInput = {
      name,
      display,
      line1,
      line2,
      postal,
      city,
      country,
      geometry,
      phone,
      email,
      website,
      description,
      category,
      tags,
      facebook,
      instagram,
      twitter,
      youtube,
    }

    this.props.addVenue(userInput, this.props.history);
  }

  onChangeVenueCategory = (category) => {
    let categories = []
    category.forEach((category) => categories.push(category.value));
    this.setState({ category: categories });
  }

  onDeleteVenueTag = (i) => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i),
    });
  }

  onChangeVenueTags = (tag) => {
    this.setState(state => ({ tags: [...state.tags, '#'+tag.id.toLowerCase()] }));
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }
  }

  render() {
    const {name, display, line1, line2, postal, city, country, geometry, phone, email, website, description, category, tags, facebook, instagram, twitter, youtube, displaySocialInput, errors} = this.state;

    // for react-select create an array of objects, in which each object contains key value pairs of 'value' & 'label'
    let categoriesObjectArray = [];
    if(category) {
      category.forEach((eachCategory) => {
        let eachCategoryObject = {}
        eachCategoryObject['value'] = eachCategory;
        eachCategoryObject['label'] = eachCategory;
        categoriesObjectArray.push(eachCategoryObject);
      })
    }

    let tagsObjectArray = [];
    if(tags) {
      tags.forEach((eachTag) => {
        let eachTagObject = {}
        eachTagObject['id'] = eachTag;
        eachTagObject['text'] = eachTag;
        tagsObjectArray.push(eachTagObject);
      })
    }

    let socialInputs;
    if(displaySocialInput) {
      socialInputs = (
          <div>
            <InputGroup
                placeholder="Facebook"
                name="facebook"
                icon="fab fa-facebook facebook"
                value={facebook}
                onChange={this.onChange}
                error={errors.facebook}
            />
            <InputGroup
                placeholder="Instagram"
                name="instagram"
                icon="fab fa-instagram instagram"
                value={instagram}
                onChange={this.onChange}
                error={errors.instagram}
            />
            <InputGroup
                placeholder="Twitter"
                name="twitter"
                icon="fab fa-twitter twitter"
                value={twitter}
                onChange={this.onChange}
                error={errors.twitter}
            />
            <InputGroup
                placeholder="Youtube"
                name="youtube"
                icon="fab fa-youtube youtube"
                value={youtube}
                onChange={this.onChange}
                error={errors.youtube}
            />
          </div>
      );
    }

    return (
      <div id="add-venue">
        <Navbar/>
        <div className="container p-0">
          <form name="add-venue-form" className="form-style m-auto" noValidate onSubmit={this.onSubmit} autoComplete="off">
            <h3 className="form-login-heading text-center">Add Venue</h3>
            <hr className="colorgraph mb-5" />

            <Select
              isMulti
              placeholder="Venue Category"
              name="category"
              options={defaultVenueCategories}
              classNamePrefix="select"
              onChange={this.onChangeVenueCategory}
              value={categoriesObjectArray}
              className={classnames('basic-multi-select form-control border-0 p-0 pb-3', {'is-invalid': errors.category})}
            />
            {errors.category && (<div className="invalid-feedback pb-3">{errors.category}</div>)}

            <div className="form-group">
              <input type="text" name="name" placeholder="Venue Name" onChange={this.onChange} value={name} className={classnames('form-control', {'is-invalid': errors.name})}/>
              {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
            </div>


            <div className="form-group">
              <input type="text" name="display" placeholder="Display Name (for gigatool)" onChange={this.onChange} value={display} className={classnames('form-control', {'is-invalid': errors.display})}/>
              {errors.display && (<div className="invalid-feedback">{errors.display}</div>)}
            </div>

            <div className="form-row">
              <div className="form-group col-md-12">
                <input type="text" name="line1" placeholder="Line1: 1234 Main Str." onChange={this.onChange} value={line1} className={classnames('form-control', {'is-invalid': errors.line1})}/>
                {errors.line1 && (<div className="invalid-feedback">{errors.line1}</div>)}
              </div>
              <div className="form-group col-md-12">
                <input type="text" name="line2" placeholder="Line2: Apartment, studio, or floor" onChange={this.onChange} value={line2} className={classnames('form-control', {'is-invalid': errors.line2})}/>
                {errors.line2 && (<div className="invalid-feedback">{errors.line2}</div>)}
              </div>

              <div className="form-group col-md-4">
                <input type="text" name="postal" placeholder="Postal" onChange={this.onChange} value={postal} className={classnames('form-control', {'is-invalid': errors.postal})}/>
                {errors.postal && (<div className="invalid-feedback">{errors.postal}</div>)}
              </div>
              <div className="form-group col-md-8">
                <input disabled="disabled" type="text" name="city" placeholder="City" onChange={this.onChange} value={city} className={classnames('form-control', {'is-invalid': errors.city})}/>
                {errors.city && (<div className="invalid-feedback">{errors.city}</div>)}
              </div>
              <div className="form-group col-md-12">
                <input disabled="disabled" type="text" name="country" placeholder="Country" onChange={this.onChange} value={country} className={classnames('form-control', {'is-invalid': errors.country})}/>
                {errors.country && (<div className="invalid-feedback">{errors.country}</div>)}
              </div>
            </div>

            <div className="form-group">
              <input type="text" name="geometry" placeholder="Latitude,Longitude i.e. 52.516399,13.377736" onChange={this.onChange} value={geometry} className={classnames('form-control', {'is-invalid': errors.geometry})}/>
              {errors.geometry && (<div className="invalid-feedback">{errors.geometry}</div>)}
            </div>

            <div className="form-group">
              <PhoneInput
                placeholder="Phone"
                value={phone}
                onChange={phone => this.setState({ phone })}
                country="DE"
                error={ phone ? (isValidPhoneNumber(phone) ? undefined : 'Invalid phone number') : '' }
              />
            </div>

            <div className="form-group">
              <input type="email" name="email" placeholder="Email" onChange={this.onChange} value={email} className={classnames('form-control', {'is-invalid': errors.email})}/>
              {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
            </div>

            <div className="form-group">
              <input type="url" pattern="https://.*" name="website" placeholder="Website i.e. https://venue.de" onChange={this.onChange} value={website} className={classnames('form-control', {'is-invalid': errors.website})}/>
              {errors.website && (<div className="invalid-feedback">{errors.website}</div>)}
            </div>

            <div className="form-group">
              <textarea rows="5" name="description" placeholder="Description" onChange={this.onChange} value={description} className={classnames('form-control', {'is-invalid': errors.description})}/>
              {errors.description && (<div className="invalid-feedback">{errors.description}</div>)}
            </div>

            <div className="form-group">
              <ReactTags
                classNames={{
                  tagInputField: 'input-tags',
                }}
                id="venue-tags-input"
                tags={tagsObjectArray}
                handleDelete={this.onDeleteVenueTag}
                handleAddition={this.onChangeVenueTags}
                delimiters={[KeyCodes.enter, KeyCodes.comma, KeyCodes.space, KeyCodes.minus, KeyCodes.hash]}
              />
            </div>

            <button
                className="btn btn-light btn-lg btn-block mb-3 bg-dark text-white"
                type="button"
                onClick={() => {
                  this.setState(prevState =>
                      ({displaySocialInput: !prevState.displaySocialInput})
                  )
                }}
            >
              Add Social Media {(displaySocialInput) ? (<i className="fas fa-chevron-up pl-2"></i>) : (<i className="fas fa-chevron-down pl-2"></i>)}
            </button>
            {socialInputs}

            <button className="btn btn-lg btn-primary btn-block my-4" type="Submit">Add Venue</button>
          </form>
        </div>
      </div>
    );
  }
}

AddVenue.propTypes = {
  errors: PropTypes.object
}

const mapStateToProps = (state) => ({
  errors: state.errors
});

export default connect(mapStateToProps, {addVenue})(withRouter(AddVenue));
