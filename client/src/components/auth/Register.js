import React, {Component} from 'react';
import classnames from 'classnames';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {registerUser} from "../../actions/authActions";

class Register extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    errors: {}
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const userInput = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
    }

    this.props.registerUser(userInput, this.props.history);
  }

  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors) {
      this.setState({errors: nextProps.errors})
    }
  }

  render() {
    const {name, email, password, errors} = this.state;

    return (
        <div id="register">
          <div className="container col-md-4 d-flex justify-content-center align-items-center" style={{'height': '100%', 'minHeight': '100vh'}}>
            <form noValidate onSubmit={this.onSubmit} autoComplete="off">
              <div className="py-2 partygps-title">#PARTYGPS</div>
              <div className="pb-5 partygps-subtitle">YOUR ONLY WAY OUT</div>

              <div className="form-group">
                <input type="text" name="name" placeholder="Company / Name" onChange={this.onChange} value={name} className={classnames('form-control partygps-text-input', {'is-invalid': errors.name})}/>
                {errors.name && (<div className="invalid-feedback">{errors.name}</div>)}
              </div>

              <div className="form-group">
                <input type="email" name="email" placeholder="Email" onChange={this.onChange} value={email} className={classnames('form-control partygps-text-input', {'is-invalid': errors.email})}/>
                {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
              </div>

              <div className="form-group">
                <input type="password" name="password" placeholder="Password" onChange={this.onChange} value={password} className={classnames('form-control partygps-text-input', {'is-invalid': errors.password})}/>
                {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
              </div>

              <button className="btn btn-lg btn-primary btn-block my-4 partygps-btn-blue" type="Submit">Register</button>
            </form>
          </div>
        </div>
    );
  }
}

Register.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, {registerUser})(withRouter(Register));
