import React, {Component} from 'react';
import classnames from 'classnames';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {loginUser, loginUserSpotify} from "../../actions/authActions";
import { AlertList } from "react-bs-notifier";
import {clearErrors} from "../../actions/errorActions";

class Login extends Component {
  state = {
    email: '',
    password: '',
    errors: {},
    alerts: [],
  }

  onChange = e => {
    this.setState({[e.target.name]:e.target.value})
  }

  onSubmit = e => {
    e.preventDefault();

    const {email, password} = this.state;

    const userInput = {
      email,
      password,
    }

    this.props.loginUser(userInput);
  }

  sotifyLogin = e => {
    e.preventDefault();
    this.props.loginUserSpotify();
  }

  componentDidMount() {
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }

    if(nextProps.errors) {
      if(nextProps.errors.alert) {
        this.setState({
          errors: nextProps.errors,
          alerts: [nextProps.errors.alert]
        });
      } else {
        this.setState({
          errors: nextProps.errors,
        });
      }
    }
  }

  render() {
    const {email, password, errors, alerts} = this.state;

    return (
        <div id="login" className="bg">
          {errors.alert &&
          (
              <div className="alert-wrapper">
                <AlertList
                    position="top-right"
                    alerts={alerts}
                    timeout={3000}
                    onDismiss={() => {this.setState({alerts: []}); this.props.clearErrors()}}
                />
              </div>
          )
          }

          <div className="container col-md-4 d-flex justify-content-center align-items-center" style={{'height': '100%', 'minHeight': '100vh'}}>
            <form name="login_form" className="form-style m-auto" noValidate onSubmit={this.onSubmit} autoComplete="off">
              <div className="py-2 partygps-title">#PARTYGPS</div>
              <div className="pb-5 partygps-subtitle">YOUR ONLY WAY OUT</div>

              <div className="form-group">
                <input type="email" name="email" placeholder="Email" onChange={this.onChange} value={email} className={classnames('form-control partygps-text-input', {'is-invalid': errors.email})}/>
                {errors.email && (<div className="invalid-feedback">{errors.email}</div>)}
              </div>

              <div className="form-group">
                <input type="password" name="password" placeholder="Password" onChange={this.onChange} value={password} className={classnames('form-control partygps-text-input', {'is-invalid': errors.password})}/>
                {errors.password && (<div className="invalid-feedback">{errors.password}</div>)}
              </div>

              <button className="btn btn-lg btn-primary btn-block my-4 partygps-btn-blue" type="Submit" id="local">Login</button>

              <a className="btn btn-lg btn-primary btn-block my-4 partygps-btn-blue spotify-login" id="spotify" href="/auth/spotify">Login with Spotify</a>
            </form>

          </div>
        </div>
    );
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
  loginUserSpotify: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, {loginUser, loginUserSpotify, clearErrors})(withRouter(Login));
