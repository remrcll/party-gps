// import modules
const express = require('express');
const router = express.Router();
const passport = require('passport');

// import files & others

// load model
const Artist = require('../models/Artist');

// @route   GET api/artist/test
// @desc    test route for artist
// @access  public
router.get('/test', (req, res) => res.status(200).json({response: '/api/artist/test : test route for artist.'}));

// @route   POST /api/venue/
// @desc    add venue data
// @access  private
router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {
  console.log('add artist data...');
  res.status(200).json({response: req.user});
});

module.exports = router;
