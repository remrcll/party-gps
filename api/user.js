// import modules
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');

// import files & others
const jwtKey = require('../config/keys').secretOrKey;
const roles = require('../config/roles');

// load model
const User = require('../models/User');

// load input validation
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');

// @route   GET api/user/test
// @desc    test route for user
// @access  public
router.get('/test', (req, res) => res.status(200).json({response: '/api/user/test : test route for user.'}));

// @route   POST api/user/register
// @desc    register new user (sign-up)
// @access  private
//@TODO make register route protected until role field is not implemented
router.post('/register', (req, res) => {
  console.log('register new user (sign-up)...');

  const {errors, isValid} = validateRegisterInput(req.body);
  const {name, email, password, phone, avatar} = req.body;

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({email})
    .then((user) => {
      if(user) {
        // user email already registered
        errors.email = 'Email is already registered.';
        return res.status(400).json(errors);
      } else {
        // create new user in database
        const newUser = new User({
          name,
          email,
          password,
          phone,
          avatar
        });

        // creating a hash for password
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if(err) throw err;
            newUser.password = hash;

            // saving newUser in database
            newUser.save()
              .then(user => res.status(200).json(user))
              .catch(err => {
                console.log(err);

                // mongoose validation
                const {name, email, phone} = err.errors;

                if(name) {
                  errors.name = name.message;
                }
                if(email) {
                  errors.email = email.message;
                }
                if(phone) {
                  errors.phone = phone.message;
                }
                return res.status(400).json(errors);
              });
          })
        })
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: 'new user registration failed.'});
    });
});

// @route   POST api/user/login
// @desc    user login
// @access  public
router.post('/login', (req, res) => {
  console.log('user login...');

  const {email, password} = req.body;
  const {errors, isValid} = validateLoginInput(req.body);

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  // find user by email
  User.findOne({email})
    .then((user) => {

      // check if email doesn't exist in database
      if(!user) {
        errors.email = 'Email is not registered.';
        return res.status(404).json(errors);
      }

      // check if user role is 'block'
      if(user.role === 'block') {
        const alert = {
          id: (new Date()).getTime(),
          type: 'danger',
          message: `${user.name}! You are blocked to perform this operation.`
        }
        errors.alert = alert;
        return res.status(400).json(errors);
      }

      // check password
      bcrypt.compare(password, user.password)
        .then(isMatch => {

          if(isMatch){
            // user matched

            // creating payload for jwt
            const payload = {
              id: user.id,
              user_id: user.user_id,
              name: user.name,
              email: user.email,
              phone: user.phone,
              avatar: user.avatar,
              role: user.role
            }

            // sign token
            jwt.sign(
              payload,
              jwtKey,
              { expiresIn: 10800 },
              (err, token) => {
                const tokenObj = {
                  success: true,
                  token: 'Bearer ' + token
                }
                return res.status(200).json(tokenObj);
              });

          } else {
            errors.password = 'Incorrect password.'
            return res.status(400).json(errors);
          }
        })
        .catch(err => {
          console.log(err);
          return res.status(400).json({response: 'user login failed.'});
        });
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: 'user login failed.'});
    });
});

// @route   GET api/user/
// @desc    jwt authenticated user
// @access  private
router.get('/', passport.authenticate('jwt', {session: false}), roles(['admin']), (req, res) => {
  console.log('jwt authenticated user...');

  const {id, user_id, name, email, phone, avatar, role} = req.user;

  return res.json({id, user_id, name, email, phone, avatar, role});
});

// @route   GET api/user/all
// @desc    get all users for admin
// @access  private
router.get('/all', passport.authenticate('jwt', {session: false}), roles(['admin']), (req, res) => {
  console.log('get all users for admin...');

  User.find()
    .then(users => {
      return res.status(200).json(users);
    })
    .catch(err => {
      return res.status(400).json({response: 'all users data fetching failed.'})
    })
});

// @route   DELETE api/user/:user_id
// @desc    delete a user for admin
// @access  private
router.delete('/:user_id', passport.authenticate('jwt', {session: false}), roles(['admin']), (req, res) => {
  console.log('delete a user for admin...');

  const {user_id} = req.params;
  const errors = {};

  if(Number(user_id) === req.user.user_id) {
    const alert = {
      id: (new Date()).getTime(),
      type: 'danger',
      message: `${req.user.name}! you can't delete your own account.`
    }
    errors.alert = alert;
    return res.status(200).json(errors);
  } else {
    User.findOneAndDelete({user_id})
      .then((user) => {
        const alert = {
          id: (new Date()).getTime(),
          type: 'success',
          message: `${user.name}! Deleted successfully.`
        }
        errors.alert = alert;
        return res.status(200).json(errors);
      })
      .catch(err => {
        console.log(err);
        return res.status(400).json({response: 'user account deletion from database failed.'});
      });
  }
});

module.exports = router;
