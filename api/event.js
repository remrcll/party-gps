// import modules
const express = require('express');
const router = express.Router();
const passport = require('passport');
const axios = require('axios');
const gigaToolsKey = require('../config/keys').gigaToolsAptKey;

// import files & others
const roles = require('../config/roles');

// load model
const Venue = require('../models/Venue');
const event = require('../models/Event');

// load input validation
const validateAddEventInput = require('../validation/addEvent');
const caseInsensitiveString = require('../validation/caseInsensitiveString');

// @route   GET api/event/test
// @desc    test route for event
// @access  public
router.get('/test', (req, res) => res.status(200).json({response: '/api/event/test : test route for event.'}));

// @route   POST /api/event/
// @desc    add event data
// @access  private
router.post('/', passport.authenticate('jwt', {session: false}), roles(['admin', 'moderator', 'creator']), (req, res) => {
    console.log('add event data...');

    const {errors, isValid} = validateAddEventInput(req.body);
    const {venue_name, name, description, start_date, start_time, end_date, end_time, line_up, media_library, tags, source_url} = req.body;

    // check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const eventFields = {};
    eventFields.author = {};
    eventFields.author.user_id = req.user.user_id;
    eventFields.author.name = req.user.name;
    eventFields.author.avatar = req.user.avatar;
    eventFields.author.role = req.user.role;
    if (name) eventFields.name = name;
    if (description) eventFields.description = description;
    if (start_date) eventFields.start_date = start_date;
    if (start_time) eventFields.start_time = start_time;
    if (end_date) eventFields.end_date = end_date;
    if (end_time) eventFields.end_time = end_time;
    if (source_url) eventFields.source_url = source_url;
    if (media_library) eventFields.media_library = media_library;
    if (tags) eventFields.tags = tags;

    // get the venue._id by using venue_name
    Venue.findOne({name: venue_name})
        .then(venue => {
            if (venue) {
                // saving the found venue._id in the venue_ref field of eventFields
                eventFields.venue_ref = venue._id;

                // find the event in database by name & venue _id
                event.findOne({$and: [{name: caseInsensitiveString(name)}, {venue_ref: eventFields.venue_ref}]})
                    .then(event => {
                        if (event) {
                            // event already exist in database
                            errors.name = `'${name}' already created for venue '${venue_name}'.`;
                            return res.status(400).json(errors);
                        } else {
                            // creating new event in database
                            new event(eventFields).save()
                                .then(newEvent => res.status(200).json(newEvent))
                                .catch(err => {
                                    // console.log(err);

                                    // mongoose validation
                                    const {venue_ref, name, description, start_date, start_time, end_date, end_time} = err.errors;

                                    if (venue_ref) {
                                        errors.venue_ref = venue_ref.message;
                                    }
                                    if (name) {
                                        errors.name = name.message;
                                    }
                                    if (description) {
                                        errors.description = description.message;
                                    }
                                    if (start_date) {
                                        if (start_date.name === 'CastError') {
                                            errors.start_date = 'Invalid Date format.';
                                        } else {
                                            errors.start_date = start_date.message;
                                        }
                                    }
                                    if (start_time) {
                                        if (start_time.name === 'CastError') {
                                            errors.start_time = 'Invalid Time format.';
                                        } else {
                                            errors.start_time = start_time.message;
                                        }
                                    }
                                    if (end_date) {
                                        if (end_date.name === 'CastError') {
                                            errors.end_date = 'Invalid Date format.';
                                        } else {
                                            errors.end_date = end_date.message;
                                        }
                                    }
                                    if (end_time) {
                                        if (end_time.name === 'CastError') {
                                            errors.end_time = 'Invalid Time format.';
                                        } else {
                                            errors.end_time = end_time.message;
                                        }
                                    }
                                    res.status(400).json(errors);
                                });
                        }
                    })
                    .catch(err => {
                        console.log(err);

                        return res.status(400).json({response: 'event creation failed.?'});
                    });
            } else {
                errors.venue_name = 'Venue name didn\'t found.';
                return res.status(404).json(errors);
            }
        })
        .catch(err => res.status(400).json({response: 'venue data didn\'t found in database.?'}));
});


// @route   POST /api/event/edit
// @desc    edit event data
// @access  private
router.post('/edit', passport.authenticate('jwt', {session: false}), roles(['admin', 'moderator', 'creator']), (req, res) => {
    console.log('edit event data...');

    const {errors, isValid} = validateAddEventInput(req.body);
    const {event_id, venue_name, name, description, start_date, start_time, end_date, end_time, line_up, media_library, tags, source_url} = req.body;

    // check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }

    const eventFields = {};
    eventFields.author = {};
    eventFields.author.user_id = req.user.user_id;
    eventFields.author.name = req.user.name;
    eventFields.author.avatar = req.user.avatar;
    eventFields.author.role = req.user.role;
    if (name) eventFields.name = name;
    if (description) eventFields.description = description;
    if (start_date) eventFields.start_date = start_date;
    if (start_time) eventFields.start_time = start_time;
    if (end_date) eventFields.end_date = end_date;
    if (end_time) eventFields.end_time = end_time;
    if (source_url) eventFields.source_url = source_url;
    if (media_library) eventFields.media_library = media_library;
    if (tags) eventFields.tags = tags;

    // get the venue._id by using venue_name
    Venue.findOne({name: venue_name})
        .then(venue => {
            if (venue) {
                // saving the found venue._id in the venue_ref field of eventFields
                eventFields.venue_ref = venue._id;

                // find the event in database by name & event _id
                // event.findOne({$and: [ { name: caseInsensitiveString(name) }, { venue_ref: eventFields.venue_ref } ] })
                event.findOneAndUpdate({$and: [{event_id}, {name: caseInsensitiveString(name)}]}, {$set: eventFields}, {new: true})

                    .then(event => {
                        if (event) {
                            const alert = {
                                id: (new Date()).getTime(),
                                type: 'success',
                                message: `Event: ${event.name} updated successfully.`
                            }
                            errors.alert = alert;
                            return res.status(200).json(errors);
                        } else {
                            const alert = {
                                id: (new Date()).getTime(),
                                type: 'danger',
                                message: 'event update failed.'
                            }
                            errors.alert = alert;
                            return res.status(400).json(errors);
                        }
                    })
                    .catch(err => {
                        const alert = {
                            id: (new Date()).getTime(),
                            type: 'danger',
                            message: 'event update failed.'
                        }
                        errors.alert = alert;
                        return res.status(400).json(errors);
                    });
            } else {
                const alert = {
                    id: (new Date()).getTime(),
                    type: 'danger',
                    message: 'Venue name didn\'t found.'
                }
                errors.alert = alert;
                return res.status(400).json(errors);
            }
        })
        .catch(err => res.status(400).json({response: 'venue data didn\'t found in database.?'}));
});

// @route   GET /api/event/:event_id
// @desc    find event data by event_id
// @access  public
router.get('/:event_id', (req, res) => {
    console.log('find event data by event_id...');

    const {event_id} = req.params;

    event.findOne({event_id})
        .populate('venue_ref', ['_id', 'venue_id', 'name', 'address', 'geometry'])
        .then((event) => {
            if (event) {
                // event data by event_id found
                return res.status(200).json(event);
            } else {
                // event data by event_id didn't found
                return res.status(404).json({response: `event data by event_id: ${event_id} didn't found in database.`});
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({response: `event data by event_id: ${event_id} didn't found in database.`});
        });
});

// @route   DELETE /api/event/:event_id
// @desc    delete event data by event_id
// @access  private
router.delete('/:event_id', passport.authenticate('jwt', {session: false}), roles(['admin']), (req, res) => {
    console.log('delete event data by event_id...');

    const {event_id} = req.params;
    const errors = {};

    event.findOneAndRemove({event_id})
        .then((event) => {
            const alert = {
                id: (new Date()).getTime(),
                type: 'success',
                message: `${event.name}! Deleted successfully.`
            }
            errors.alert = alert;
            return res.status(200).json(errors);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({response: 'event data deletion from database failed.'});
        });
});

// @route   GET /api/event
// @desc    fetch all events data
// @access  public
router.get('/', (req, res) => {
    console.log('fetch all events data...');

    // event.find().sort([['event_id', -1]])
    //     .populate('venue_ref', ['_id', 'venue_id', 'name', 'address', 'geometry'])
    //     .then(events => {
    //         if (events) {
    //             return res.status(200).json(events);
    //         } else {
    //             return res.status(404).json({response: 'no event data found.'});
    //         }
    //     })
    //     .catch(err => {
    //         console.log(err);
    //         return res.status(400).json({response: 'no event data found.'});
    //     });

    // gigaToolsApi events data
    axios.get('http://api.gigatools.com/city.json?cities[]=Berlin&api_key=' + gigaToolsKey)
        .then(data => {
            const gigaToolsEventsData = data.data[1];
            return res.status(200).json(gigaToolsEventsData);
        })
        .catch(err => {
            return res.status(400).json({response: 'no event data found.'});
        });
});

// @route   GET /api/event/v/:venue_id
// @desc    events tagged with venue_id
// @access  private
router.get('/v/:venue_id', (req, res) => {
    console.log('events tagged with venue_id...');

    const today = new Date();
    event.find({$and: [{venue_ref: req.params.venue_id}, {start_date: {$gte: today.setHours(0, 0, 0, 0)}}]})
        .sort([['date', 1]])
        .then(events => {
            if (events.length > 0) {
                return res.status(200).json(events);
            } else {
                return res.status(400).json({response: 'no event data found.'});
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json({response: 'no event data found.'});
        });
});

// @route   GET /api/event/max/id
// @desc    return max value of event_id
// @access  private
router.get('/max/id', passport.authenticate('jwt', {session: false}), roles(['admin', 'moderator']), (req, res) => {
    console.log('return max value of event_id...');

    event.findOne().sort([['event_id', -1]])
        .then(event => {
            if (event) {
                return res.status(200).json(event.event_id);
            } else {
                return res.status(404).json(null);
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).json(null);
        });
});

module.exports = router;
