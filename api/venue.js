// import modules
const express = require('express');
const router = express.Router();
const passport = require('passport');

// import files & others
const roles = require('../config/roles');

// load model
const Venue = require('../models/Venue');
const Event = require('../models/Event');

// load input validation
const validateAddVenueInput = require('../validation/addVenue');
const caseInsensitiveString = require('../validation/caseInsensitiveString');

// @route   GET api/venue/test
// @desc    test route for venue
// @access  public
router.get('/test', (req, res) => res.status(200).json({response: '/api/venue/test : test route for venue.'}));

// @route   POST /api/venue/
// @desc    add venue data
// @access  private
router.post('/', passport.authenticate('jwt', {session: false}), roles(['admin', 'moderator', 'creator']), (req, res) => {
  console.log('add venue data...');

  const {errors, isValid} = validateAddVenueInput(req.body);
  const {name, display, line1, line2, postal, city, country, geometry, phone, email, avatar, website, description, category, tags, facebook, instagram, twitter, youtube} = req.body;

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  const venueFields = {};
  if(name) venueFields.name = name;
  if(display) venueFields.display = display;
  if(phone) venueFields.phone = phone;
  if(email) venueFields.email = email;
  if(avatar) venueFields.avatar = avatar;
  if(website) venueFields.website = website;
  if(description) venueFields.description = description;
  if(category) venueFields.category = category;
  if(tags) venueFields.tags = tags;

  if(geometry) {
    const coordinates = geometry.split(',').map(Function.prototype.call, String.prototype.trim);
    venueFields.geometry = coordinates;
  }

  venueFields.address = {};
  if(line1) venueFields.address.line1 = line1;
  if(line2) venueFields.address.line2 = line2;
  if(postal) venueFields.address.postal = postal;
  if(city) venueFields.address.city = city;
  if(country) venueFields.address.country = country;

  venueFields.social_media = {};
  if(facebook) venueFields.social_media.facebook = facebook;
  if(instagram) venueFields.social_media.instagram = instagram;
  if(twitter) venueFields.social_media.twitter = twitter;
  if(youtube) venueFields.social_media.youtube = youtube;

  Venue.findOne({name: caseInsensitiveString(name)})
    .then(venue => {
      if(venue) {
        // venue already exist in database
        errors.name = 'venue already exit in database.';
        return res.status(400).json(errors);
      } else {
        // create new venue in database
        new Venue(venueFields).save()
          .then(newVenue => {
            // res.status(200).json(newVenue);
            const alert = {
              id: (new Date()).getTime(),
              type: 'success',
              message: `${newVenue.name} created successfully.`
            }
            errors.alert = alert;
            return res.status(200).json(errors);
          })
          .catch(err => {
            console.log(err);

            // geometry field validation
            if(err.name === 'MongoError') {
              errors.geometry = 'Invalid Coordinates.';
              return res.status(400).json(errors);
            }

            // mongoose validation
            const {name, geometry, phone} = err.errors;

            if(name) {
              errors.name = name.message;
            }
            if(err.errors['address.line1']) {
              errors.line1 = err.errors['address.line1'].message;
            }
            if(err.errors['address.postal']) {
              if(err.errors['address.postal'].name === 'CastError') {
                errors.postal = 'Postal code should be a number.';
              } else {
                errors.postal = err.errors['address.postal'].message;
              }
            }
            if(err.errors['address.city']) {
              errors.city = err.errors['address.city'].message;
            }
            if(err.errors['address.country']) {
              errors.country = err.errors['address.country'].message;
            }
            if(geometry) {
              if(geometry.name === 'CastError') {
                errors.geometry = 'Coordinates should be number.';
              } else {
                errors.geometry = geometry.message;
              }
            }
            if(phone) {
              errors.phone = phone.message;
            }
            return res.status(400).json(errors);
          });
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: 'venue data saving in database failed.'});
    });
});

// @route   POST /api/venue/edit
// @desc    edit venue data
// @access  private
router.post('/edit', passport.authenticate('jwt', {session: false}), roles(['admin', 'moderator']), (req, res) => {
  console.log('edit venue data...');

  // console.log('user req body backend: ', req.body);

  const {errors, isValid} = validateAddVenueInput(req.body);
  const {venue_id, name, display, line1, line2, postal, city, country, geometry, phone, email, avatar, website, description, category, tags, facebook, instagram, twitter, youtube} = req.body;

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  const venueFields = {};
  if(name) venueFields.name = name;
  if(display) venueFields.display = display;
  if(phone) venueFields.phone = phone;
  if(email) venueFields.email = email;
  if(avatar) venueFields.avatar = avatar;
  if(website) venueFields.website = website;
  if(description) venueFields.description = description;
  if(category) venueFields.category = category;
  if(tags) venueFields.tags = tags;

  if(geometry) {
    const coordinates = geometry.split(',').map(Function.prototype.call, String.prototype.trim);
    venueFields.geometry = coordinates;
  }

  venueFields.address = {};
  if(line1) venueFields.address.line1 = line1;
  if(line2) venueFields.address.line2 = line2;
  if(postal) venueFields.address.postal = postal;
  if(city) venueFields.address.city = city;
  if(country) venueFields.address.country = country;

  venueFields.social_media = {};
  (facebook) ? venueFields.social_media.facebook = facebook : venueFields.social_media.facebook = '';
  (instagram) ? venueFields.social_media.instagram = instagram : venueFields.social_media.instagram = '';
  (twitter) ? venueFields.social_media.twitter = twitter : venueFields.social_media.twitter = '';
  (youtube) ? venueFields.social_media.youtube = youtube : venueFields.social_media.youtube = '';

  Venue.findOneAndUpdate({$and: [ {venue_id}, {name: caseInsensitiveString(name)} ]}, {$set: venueFields}, {new: true})
      .then(updatedVenue => {
        if(updatedVenue) {
          const alert = {
            id: (new Date()).getTime(),
            type: 'success',
            message: 'venue updated successfully.'
          }
          errors.alert = alert;
          return res.status(200).json(errors);
        } else {
          const alert = {
            id: (new Date()).getTime(),
            type: 'danger',
            message: 'venue update failed.'
          }
          errors.alert = alert;
          return res.status(400).json(errors);
        }
      })
      .catch(err => {
        console.log(err);

        const alert = {
          id: (new Date()).getTime(),
          type: 'danger',
          message: 'venue update failed.'
        }
        errors.alert = alert;
        return res.status(400).json(errors);
      });
});

// @route   GET /api/venue/:venue_id
// @desc    find venue data by venue_id
// @access  public
router.get('/:venue_id', (req, res) => {
  console.log('find venue data by venue_id...');

  const {venue_id} = req.params;

  Venue.findOne({venue_id})
    .then((venue) => {
      if(venue) {
        // venue data by _id found
        return res.status(200).json(venue);
      } else {
        // venue data by _id didn't found
        return res.status(404).json({response: `venue data by venue_id: ${venue_id} didn't found in database.`});
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: `venue data by venue_id: ${venue_id} didn't found in database.`});
    });
});

// @route   DELETE /api/venue/:venue_id
// @desc    delete venue data by venue_id
// @access  private
router.delete('/:venue_id', passport.authenticate('jwt', {session: false}), roles(['admin']), (req, res) => {
  console.log('delete venue data by venue_id...');

  const {venue_id} = req.params;
  const errors = {};

  Venue.findOne({venue_id})
    .then((venue) => {
      //checking events tagged with the venue before deleting a club
      Event.findOne({venue_ref: venue._id})
        .then(event => {
          if(event) {
            const alert = {
              id: (new Date()).getTime(),
              type: 'danger',
              message: `You cant delete '${venue.name}', because '${event.name}' is tagged with this venue.`
            }
            errors.alert = alert;
            return res.status(200).json(errors);
          } else {
            venue.remove();
            const alert = {
              id: (new Date()).getTime(),
              type: 'success',
              message: `${venue.name}! Deleted successfully.`
            }
            errors.alert = alert;
            return res.status(200).json(errors);
          }
        })
        .catch(err => res.status(400).json({response: 'Error in finding tagged events with venues.'}))
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: 'venue data deletion from database failed.'});
    });
});

// @route   GET /api/venue
// @desc    fetch all venues data
// @access  public
router.get('/', (req, res) => {
  console.log('fetch all venues data...');

  Venue.find().sort([['venue_id', -1]])
    .then(venues => {
      if(venues) {
        return res.status(200).json(venues);
      } else {
        return res.status(404).json({response: 'no venue data found.'});
      }
    })
    .catch(err => {
      console.log(err);
      return res.status(400).json({response: 'no venue data found.'});
    });
});

module.exports = router;
