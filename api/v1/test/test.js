// imports packages
const express = require('express');
const router = express.Router();

// import files & others
const alert = require('../../../util/alert');
const mail = require('../../../config/smtp_mail');

// @route   GET /api/v1/test/
// @desc    api v1 test route
// @access  public
router.get('/', (req, res) => {
  console.log('api v1 test route...');

  return res.status(200).json({'test': 'api v1 test route works!'});
});

// @route   GET /api/v1/test/alert
// @desc    api v1 alert test
// @access  public
router.get('/alert', (req, res) => {
  console.log('api v1 alert test...');

  return res.status(200).json(alert('success', 'api v1 alert test works!'));
});

// @route   GET /api/v1/test/email
// @desc    api v1 email test
// @access  public
router.get('/email', (req, res) => {
  console.log('api v1 email test...');

  const data = {
    from: `PartyGPS ${mail.mail_user}`,
    to: mail.mail_user,
    subject: `Test Mail`,
    html: '<p>This is a test email from Admin PartyGPS!</p>',
  };

  mail.smtpTransport.sendMail(data, function(err) {
    if (!err) {
      return res.status(200).json({ status: 'success', message: 'Please check your email, api v1 test email sent successfully!' });
    } else {
      return res.status(400).json(err);
    }
  });
});

module.exports = router;

