// import packgs
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');

// import files & others
const jwtKey = require('../../../config/keys').secretOrKey;
const roles = require('../../../config/roles');
const protectedRoute = passport.authenticate('jwt', {session: false});
const alert = require('../../../util/alert');

// load models
const User = require('../../../models/User');

// @route   GET /user/all
// @desc    get all users
// @access  private
// @access  @TODO
router.get('/all', protectedRoute/*, roles(['admin'])*/, (req, res) => {
  console.log('get all users...');

  User.find()
      .then(users => {
        return res.status(200).json(users);
      })
      .catch(err => res.status(400).json(err));
});

// @route   GET /user/:user_id
// @desc    get a user with user_id
// @access  private
router.get('/:user_id', protectedRoute, roles(['active', 'admin']), (req, res) => {
  console.log('get a user with user_id...');
  const {user_id} = req.params;

  User.findOne({user_id})
      .then(user => {
        return res.status(200).json(user);
      })
      .catch(err => res.status(400).json(err));
});

// @route   GET /user/
// @desc    get authenticated user
// @access  private
router.get('/', protectedRoute, (req, res) => {
  console.log('get authenticated user...');
  const {id, user_id, name, email, handle, avatar, role} = req.user;

  return res.json({id, user_id, name, email, handle, avatar, role});
});

// @route   DELETE /user/:user_id
// @desc    delete a user with user_id
// @access  private
router.delete('/:user_id', protectedRoute, roles(['admin']), (req, res) => {
  console.log('delete a user...');
  const {user_id} = req.params;
  const errors = {};

  if(Number(user_id) === req.user.user_id) {
    return res.status(400).json(alert('danger', `${req.user.name}! you can't delete your own account.`));
  } else {
    User.findOneAndDelete({user_id})
        .then((user) => {
          return res.status(200).json(alert('success', `${user.name}! Deleted successfully.`));
        })
        .catch(err => res.status(400).json(err));
  }
});

module.exports = router;
