// import packgs
const express = require('express');
const router = express.Router();
const passport = require('passport');
const nodemailer = require('nodemailer');
const async = require('async');
const crypto = require('crypto');
const path = require('path');
const bcrypt = require('bcryptjs');

// import files & others
const protectedRoute = passport.authenticate('jwt', {session: false});
const roles = require('../../../config/roles');
const host = require('../../../config/keys').host;
const mail_user = require('../../../config/keys').gmail.user;
const mail_pass = require('../../../config/keys').gmail.pass;
const mail_host = require('../../../config/keys').gmail.host;
const mail_port = require('../../../config/keys').gmail.port;

// load models
const User = require('../../../models/User');

// load input validation
const validateForgotPasswordInput = require('../../../validation/forgot_password');
const validateResetPasswordInput = require('../../../validation/reset_password');

// nodemailer settings
const smtpTransport = nodemailer.createTransport({
  auth: {
    user: mail_user,
    pass: mail_pass
  },
  host: mail_host,
  port: mail_port,
  secure: false,
});

// @route   GET /reset/test
// @desc    test email route for admin
// @access  private
router.get('/test', protectedRoute, roles(['admin']), (req, res) => {
  console.log('test email route for admin...');
  const data = {
    from: `TimeTracker ${mail_user}`,
    to: mail_user,
    subject: `Test Mail`,
    html: '<p>This is a test email from Admin TimeTracker!</p>',
  };
  smtpTransport.sendMail(data, function(err) {
    if (!err) {
      return res.status(200).json({ status: 'success', message: 'Please check your email, you got a password reset link.' });
    } else {
      return res.status(400).json(err);
    }
  });
});

// @route   POST /reset/forgot
// @desc    email the forgot password link to user
// @access  public
router.post('/forgot', (req, res) => {
  console.log('email the forgot password link to user...');
  const { errors, isValid } = validateForgotPasswordInput(req.body);

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  async.waterfall([
    // 1. find the user entered email in database
    function(done) {
      console.log('waterfall 1...');
      User.findOne({email: req.body.email})
          .then( user => {
            if(user) {
              done(null, user);
            } else {
              errors.email = 'Email is not registered.';
              return res.status(404).json(errors);
            }
          })
          .catch(err => res.status(400).json(err));
    },
    // 2. creating a random token to generate a password reset url
    function(user, done) {
      console.log('waterfall 2...');
      crypto.randomBytes(20, (err, buffer) => {
        const token = buffer.toString('hex');
        done(null, user, token);
      });
    },
    // 3. saving the token in database for the user by its _id
    function(user, token, done) {
      console.log('waterfall 3...');
      User.findOneAndUpdate({ _id: user._id }, { reset_password_token: token, reset_password_expires: Date.now() + 600000 }, { upsert: true, new: true })
          .then(user => {
            done(null, token, user);
          })
          .catch(err => res.status(400).json(err));
    },
    // 4. send the url to the user email address
    function(token, user, done) {
      console.log('waterfall 4...');
      const resetLink = `${host}reset-password/token=${token}`;

      const data = {
        from: `TimeTracker ${mail_user}`,
        to: user.email,
        subject: 'Password Reset Link',
        html: '<div class="forgot-password">' +
                '<p>Hi <strong style="color: red">' + user.name + '</strong>!</p>' +
                'Here is your password reset link: <a href="' + resetLink + '">CLICK</a>' +
              '</div>',
      };
      smtpTransport.sendMail(data, function(err) {
        if (!err) {
          console.log(`forgot password link set to: ${user.email} successfully...`);
          return res.status(200).json({ status: 'success', message: token });
        } else {
          console.log('error in sending forgot password link...');
          done(err);
        }
      });
    }
  ], function(err) {
    console.log('waterfall end with err...');
    return res.status(422).json({ status: 'danger', message: 'Password Reset Link Email sent failed!' });
  });
});

// @route   POST /reset/password
// @desc    reset new password, directed link from email
// @access  public
router.post('/password', (req, res) => {
  console.log('reset new password, directed link from email...');
  const { errors, isValid } = validateResetPasswordInput(req.body);

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }
  User.findOne({
    reset_password_token: req.body.token,
    reset_password_expires: {
      $gt: Date.now()
    }
  })
  .then(user => {
    if(user) {
      user.password = bcrypt.hashSync(req.body.newPassword1, 10);
      user.reset_password_token = '';
      user.reset_password_expires = undefined;
      // updating user in database (reset_password_token & reset_password_expires)
      user.save()
          .then(user => res.status(200).json({ status: 'success', user }))
          .catch(err => res.status(422).json(err));
    } else {
      // null user or error
      return res.status(404).json({ status: 'fail', msg: 'password update failed.' });
    }
  })
  .catch(err => res.status(400).json(err));
});

module.exports = router;
