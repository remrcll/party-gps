// import packages
const express = require('express');
const router = express.Router();
const passport = require('passport');
const queryString = require('querystring');
const jwt = require('jsonwebtoken');
const axios = require('axios');

// import files & others
const jwtKey = require('../../../../config/keys').secretOrKey;
const spotify = require('../../../../config/keys').spotify;
const host = require('../../../../config/keys').host;
const generateRandomString = require('../../../../util/generate_random_string');

// load models
const User = require('../../../../models/User');

// @route   GET /auth/spotify
// @desc    spotify user login
// @access  public
router.get(
    '/',
    passport.authenticate('spotify', {
      scope: spotify.scope.split(' ')
    }),
    function(req, res) {});

// @route   GET /auth/spotify/callback
// @desc    spotify callback response
// @access  public
router.get('/callback', passport.authenticate('spotify', { failureRedirect: '/auth/spotify/' }),
    function(req, res) {
      console.log('spotify callback response...');
      const {name, email, avatar, accessToken, refreshToken} = req.user;

      // find spotify user in our database
      User.findOne({email}, function(err, user) {
        if (err) {
          return res.status(400).json({response: 'Database error.'});
        }
        if(!user) {
          // spotify user not found
          // creating new user in database
          const newUser = new User({name, email, avatar});

          // saving new user
          newUser.save()
              .then(user => {
                // user saved successfully in our database
                // create jwt token for newly created user and
                // then redirect to frontend app
                createJwtToken(user, accessToken, refreshToken, (token) =>
                    res.redirect(`${host}spotify/#${queryString.stringify({token: token})}`));
              })
              .catch(err => {
                // mongoose validation
                console.log('mongoose validation error: ', err);
                const {name, email} = err.errors;

                if(name) {
                  errors.name = name.message;
                }
                if(email) {
                  errors.email = email.message;
                }
                return res.status(400).json(errors);
              });

        } else {
          // user found in our database
          // create jwt token for newly created user and
          // then redirect to frontend app
          createJwtToken(user, accessToken, refreshToken, (token) =>
              res.redirect(`${host}spotify/#${queryString.stringify({token: token})}`));
        }
      });
    }
);

// create payload with user data, access_token and refresh_token
// and then create a jwtToken to retuen to frontend app
function createJwtToken(user, access_token, refresh_token, cb) {
  // create jwt token with user data
  const payload = {
    id: user.id,
    user_id: user.user_id,
    name: user.name,
    email: user.email,
    phone: user.phone,
    avatar: user.avatar,
    role: user.role,
    access_token: access_token,
    refresh_token: refresh_token,
  }

  // sign token
  jwt.sign(
      payload,
      jwtKey,
      { expiresIn: 3600 },
      (err, token) => {
        token = 'Bearer ' + token;
        cb(token);
      });
}

/*
 * custom code to authorize with spotify api
 *
  // @route   GET /auth/spotify
  // @desc    spotify user login
  // @access  public
  router.get('/', function(req, res) {
    console.log('spotify user login...');

    const stateValue = generateRandomString(16);
    res.cookie(spotify.stateKey, stateValue);

    // send request to spotify to get authorization code:
    // if user login successfully with spotify, then user will be redirected to
    // the callback url route '/auth/spotify/callback'
    res.redirect(spotify.authURL +
        queryString.stringify({
          response_type: 'code',
          client_id: spotify.clientID,
          redirect_uri: spotify.callbackURL,
          scope: spotify.scope,
          state: stateValue,
    }));
  });

  // @route   GET /auth/spotify/callback
  // @desc    spotify callback response
  // @access  public
  router.get('/callback', function(req, res) {
    console.log('spotify callback response...');

    const errors = {};
    const authorizationCode = req.query.code || null;
    const state = req.query.state || null;
    const storedState = req.cookies ? req.cookies[spotify.stateKey] : null;

    if (state === null || state !== storedState) {
      res.status(400).json({response: 'state_mismatch'})
    } else {
      res.clearCookie(spotify.stateKey);

      const getTokenConfigOptions = {
        url: spotify.tokenURL,
        method: 'post',
        params: {
          code: authorizationCode,
          redirect_uri: spotify.callbackURL,
          grant_type: spotify.grantType,
        },
        headers: {
          'Authorization': `Basic ${(new Buffer(spotify.clientID + ':' + spotify.clientSecret).toString('base64'))}`,
        },
        responseType: 'json',
      }

      // request to spotify access token api route with the authorization code that
      // was provided to callback route by the spotify response of successful user
      // login '/auth/spotify'
      axios(getTokenConfigOptions)
          .then(response => {
            if(response.status === 200) {
              const { access_token, refresh_token } = response.data;

              // get spotify user data to create and return frontend app jwt token
              const getUserDataConfigOptions = {
                url: spotify.profileURL,
                method: 'post',
                headers: {
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
                  'Access-Control-Allow-Origin': '*',
                  'Authorization': 'Bearer ' + access_token
                },
              }
              axios(getUserDataConfigOptions)
                  .then(response => {
                    if(response.status === 200) {
                      const {display_name, email} = response.data;

                      // check if the spotify user exist in partygps database
                      // if not create new user otherwise update it into our database
                      User.findOne({email})
                          .then((user) => {
                            if(user) {
                              // @TODO
                              // user email exist in database
                              // update user profile in database
                              console.log('spotify user exist...');
                              createJwtToken(user, access_token, refresh_token, (token) =>
                                  res.redirect(`${host}spotify/#${queryString.stringify({token: token})}`));
                            } else {
                              // create new user in database
                              const newUser = new User({
                                name: display_name,
                                email: email,
                              });

                              // saving newUser in database
                              newUser.save()
                                  .then(user => {
                                    // create jwt token for newly created user and
                                    // then redirect to frontend app
                                    console.log('new spotify user created...');
                                    createJwtToken(user, access_token, refresh_token, (token) =>
                                        res.redirect(`${host}spotify/#${queryString.stringify({token: token})}`));
                                  })
                                  .catch(err => {
                                    // mongoose validation
                                    console.log('mongoose validation error: ', err);
                                    const {name, email} = err.errors;

                                    if(name) {
                                      errors.name = name.message;
                                    }
                                    if(email) {
                                      errors.email = email.message;
                                    }
                                    return res.status(400).json(errors);
                                  });
                            }
                          })
                          .catch(err => res.status(400).json(err));
                    }
                  })
                  .catch(err => {
                    console.log(err);
                  });
            }
          })
          .catch(err => {
            res.status(400).json({response: 'Invalid token.'});
          });
    }
  });

*/

module.exports = router;
