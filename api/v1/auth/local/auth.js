// import packages
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');

// import files & others
const jwtKey = require('../../../../config/keys').secretOrKey;
const roles = require('../../../../config/roles');
const alert = require('../../../../util/alert');

// load models
const User = require('../../../../models/User');

// load input validation
const validateRegisterInput = require('../../../../validation/register');
const validateLoginInput = require('../../../../validation/login');

// @route   POST /auth/local/login
// @desc    user login
// @access  public
router.post('/login', (req, res) => {
  console.log('user login...');

  const {email, password} = req.body;
  const {errors, isValid} = validateLoginInput(req.body);

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  // find user by email
  User.findOne({email})
      .then(user => {

        // check if email doesn't exist in database
        if(!user) {
          errors.email = 'Email is not registered.';
          return res.status(404).json(errors);
        }

        // check if user role is 'block'
        if(user.role === 'block') {
          return res.status(400).json(alert('danger', `${user.name}! You are blocked to perform this operation.`));
        }

        // check password
        bcrypt.compare(password, user.password)
            .then(isMatch => {

              console.log(password, user.password, isMatch);

              if(isMatch){
                // user matched - creating payload for jwt
                const payload = {
                  id: user.id,
                  user_id: user.user_id,
                  name: user.name,
                  email: user.email,
                  phone: user.phone,
                  avatar: user.avatar,
                  role: user.role,
                }
                // sign token
                jwt.sign(
                    payload,
                    jwtKey,
                    { expiresIn: 10800 },
                    (err, token) => {
                      const tokenObj = {
                        success: true,
                        token: 'Bearer ' + token
                      }
                      return res.status(200).json(tokenObj);
                    });
              } else {
                errors.password = 'Incorrect password.'
                return res.status(400).json(errors);
              }
            })
            .catch(err => {
              errors.password = 'Incorrect password.'
              return res.status(400).json(errors);
            });
      })
      .catch(err => res.status(400).json(err));
});

// @route   POST /auth/local/register
// @desc    register new user (sign-up)
// @access  public
router.post('/register', (req, res) => {
  console.log('register new user (sign-up)...');

  const {errors, isValid} = validateRegisterInput(req.body);
  const {name, email, password} = req.body;

  // check validation
  if(!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({email})
      .then((user) => {
        if(user) {
          // user email is already taken
          errors.email = 'Email is already taken.';
          return res.status(400).json(errors);
        } else {
          // validating password before hashing
          const pass_len = password.length;
          if(pass_len === 0 || password === '') {
            errors.password = 'password is required...';
            return res.status(400).json(errors);
          } else if(pass_len < 6 || pass_len > 30) {
            errors.password = 'password length should be 2-30 ch...';
            return res.status(400).json(errors);
          }

          // create new user in database
          const newUser = new User({
            name,
            email,
            password,
          });

          // creating a hash for password
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if(err) throw err;
              newUser.password = hash;

              // saving newUser in database
              newUser.save()
                  .then(user => res.status(200).json(user))
                  .catch(err => {
                    // mongoose validation
                    console.log('mongoose validation error: ', err);
                    const {name, email} = err.errors;

                    if(name) {
                      errors.name = name.message;
                    }
                    if(email) {
                      errors.email = email.message;
                    }
                    return res.status(400).json(errors);
                  });
            })
          })
        }
      })
      .catch(err => res.status(400).json(err));
});

module.exports = router;
