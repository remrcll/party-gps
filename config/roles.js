// load models
const User = require('../models/User');

module.exports = roles => {
  const errors = {};

  return function(req, res, next){
    const {_id} = req.user;

    User.findById(_id, (err, user) => {
      if(err) {
        return res.status(422).json({error: 'No user found.'});
      }

      if(roles.indexOf(user.role) > -1) {
        return next();
      }

      const alert = {
        id: (new Date()).getTime(),
        type: 'danger',
        message: `${user.name}! You are blocked to perform this operation.`
      }
      errors.alert = alert;
      return res.status(400).json(errors);
    });
  }
}
