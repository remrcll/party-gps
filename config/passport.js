// import packages
const ExtractJwt = require('passport-jwt').ExtractJwt;
const JwtStrategy = require('passport-jwt').Strategy;
const SpotifyStrategy = require('passport-spotify').Strategy;

// import files & others
const jwtKey = require('./keys').secretOrKey;
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = jwtKey;
const spotify = require('./keys').spotify;
const spotifyOptions = {
  clientID: spotify.clientID,
  clientSecret: spotify.clientSecret,
  callbackURL: spotify.callbackURL,
}

// load models
const User = require('../models/User');

module.exports = (passport) => {
  //serialize the user
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  //deserialize the user
  passport.deserializeUser(function(userObject, done) {
    done(null, userObject);
  });

  // JwtStrategy
  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      console.log('JwtStrategy...');
      User.findById(jwt_payload.id)
        .then(user => {
          if(user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(err => {
          console.log(err);
          return res.status(400).json({response: 'jwt authentication failed.'});
        });
    })
  );

  // SpotifyStrategy
  passport.use(
    new SpotifyStrategy(spotifyOptions, (accessToken, refreshToken, expires_in, profile, done) => {
      console.log('SpotifyStrategy...');
      const { email } = profile._json;
      let avatar = '';
      (profile.photos.length) ? avatar = profile.photos[0] : avatar = '';

      const spotifyResponse = {
        name: profile.id,
        email,
        avatar,
        accessToken,
        refreshToken,
      }
      done(null, spotifyResponse);
    })
  );
};
