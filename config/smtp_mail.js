// import packages
const nodemailer = require('nodemailer');

// import files & others
const mail_user = require('./keys').gmail.user;
const mail_pass = require('./keys').gmail.pass;
const mail_host = require('./keys').gmail.host;
const mail_port = require('./keys').gmail.port;

// nodemailer settings
const smtpTransport = nodemailer.createTransport({
  auth: {
    user: mail_user,
    pass: mail_pass
  },
  host: mail_host,
  port: mail_port,
  secure: false,
});

module.exports = {
  mail_user: mail_user,
  smtpTransport: smtpTransport,
};


