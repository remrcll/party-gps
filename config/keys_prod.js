module.exports = {
  mongoURI: process.env.MONGO_URI,
  secretOrKey: process.env.SECRET_OR_KEY,
  host: process.env.HOST,
  gigaToolsAptKey: process.env.GIGATOOL_API_KEY,
  gmail: {
    user: process.env.GMAIL_USER,
    pass: process.env.GMAIL_PASS,
    host: process.env.GMAIL_HOST,
    port: process.env.GMAIL_PORT,
  },
  spotify: {
    clientID: process.env.SPOTIFY_CLIENT_ID,
    clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
    authURL: process.env.SPOTIFY_AUTH_URL,
    tokenURL: process.env.SPOTIFY_TOKEN_URL,
    profileURL: process.env.SPOTIFY_PROFILE_URL,
    callbackURL: process.env.SPOTIFY_CALLBACK_URL,
    stateKey: process.env.SPOTIFY_STATE_KEY,
    grantType: process.env.SPOTIFY_GRANT_TYPE,
    scope: process.env.SPOTIFY_SCOPE,
  },
}
