// import modules
const Validator = require('validator');
const isEmpty = require('./isEmpty');
const coordinate = require('./coordinates');

module.exports = function validateAddVenueInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name.trim() : '';
  data.line1 = !isEmpty(data.line1) ? data.line1.trim() : '';
  data.postal = !isEmpty(data.postal) ? data.postal.trim() : '';
  data.city = !isEmpty(data.city) ? data.city.trim() : '';
  data.country = !isEmpty(data.country) ? data.country.trim() : '';
  data.geometry = !isEmpty(data.geometry) ? data.geometry.trim() : '';
  data.email = !isEmpty(data.email) ? data.email.toLowerCase().trim() : '';
  data.category = !isEmpty(data.category) ? data.category : null;

  // name validation
  if(!Validator.isLength(data.name, {min: 2})){
    errors.name = "Name must be at-least 2 characters long."
  }
  if(Validator.isEmpty(data.name)) {
    errors.name = 'Venue name is required';
  }

  // address: line1 validation
  if(!Validator.isLength(data.line1, {min: 2})){
    errors.line1 = "Address: line1 must be at-least 2 characters long."
  }
  if(Validator.isEmpty(data.line1)) {
    errors.line1 = 'Address: line1 is required.';
  }

  // address: postal validation
  if(Validator.isEmpty(data.postal)) {
    errors.postal = 'Postal is required.';
  }
  //@TODO number check

  // address: city validation
  if(Validator.isEmpty(data.city)) {
    errors.city = 'Address: City is required.';
  }

  // address: country validation
  if(Validator.isEmpty(data.country)) {
    errors.country = 'Address: Country is required.';
  }

  // geometry validation
  const coordinates = coordinate(data.geometry.split(',')[0], data.geometry.split(',')[1]);
  if(!coordinates) {
    errors.geometry = 'Invalid coordinates.';
  }
  if(Validator.isEmpty(data.geometry)) {
    errors.geometry = 'Coordinates are required.';
  }

  // email validation
  if(data.email !== '') {
    if(!Validator.isEmail(data.email)) {
      errors.email = 'Email is invalid.';
    }
  }

  // category validation
  if(!data.category) {
    errors.category = 'Venue category is required.';
  }

  // social media url validation
  if(!isEmpty(data.facebook)){
    if(!Validator.isURL(data.facebook)){
      errors.facebook = "Not a valid Url.";
    }
  }
  if(!isEmpty(data.instagram)){
    if(!Validator.isURL(data.instagram)){
      errors.instagram = "Not a valid Url.";
    }
  }
  if(!isEmpty(data.twitter)){
    if(!Validator.isURL(data.twitter)){
      errors.twitter = "Not a valid Url.";
    }
  }
  if(!isEmpty(data.youtube)){
    if(!Validator.isURL(data.youtube)){
      errors.youtube = "Not a valid Url.";
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
