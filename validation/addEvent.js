// import modules
const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateAddEventInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name.trim() : '';
  data.description = !isEmpty(data.description) ? data.description.trim() : '';
  data.start_date = !isEmpty(data.start_date) ? data.start_date : null;
  data.start_time = !isEmpty(data.start_time) ? data.start_time : null;
  data.end_date = !isEmpty(data.end_date) ? data.end_date : null;
  data.end_time = !isEmpty(data.end_time) ? data.end_time : null;

  // name validation
  if(!Validator.isLength(data.name, {min: 2})){
    errors.name = "Name must be at-least 2 characters long."
  }
  if(Validator.isEmpty(data.name)) {
    errors.name = 'Event name is required.';
  }

  // description validation
  if(!Validator.isLength(data.description, {min: 2})){
    errors.description = "Description must be at-least 2 characters long."
  }
  if(Validator.isEmpty(data.description)) {
    errors.description = 'Event description is required.';
  }

  // start_date validation
  if(data.start_date === null) {
    errors.start_date = 'Event start date is required.';
  } else {
    if(!Validator.isISO8601(data.start_date)) {
      errors.start_date = 'Invalid Time format.';
    }
  }

  // start_time validation
  if(data.start_time === null) {
    errors.start_time = 'Event start time is required.';
  } else {
    if(!Validator.isISO8601(data.start_time)) {
      errors.start_time = 'Invalid Time format.';
    }
  }

  // end_date validation
  if(data.end_date !== null) {
    if(!Validator.isISO8601(data.end_date)) {
      errors.end_date = 'Invalid Date format.';
    }
  }

  // end_time validation
  if(data.end_time !== null) {
    if(!Validator.isISO8601(data.end_time)) {
      errors.end_time = 'Invalid Time format.';
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}