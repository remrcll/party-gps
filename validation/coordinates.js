check_lat = /^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/;
check_lon = /^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/;

/*
* This is a global function, used for checking if
* passed coordinates are correct
* */

const coordinates = (lat, lng) => {
  const validLat = check_lat.test(lat);
  const validLng = check_lon.test(lng);
  return (validLat && validLng) ? true : false;
}

module.exports = coordinates;