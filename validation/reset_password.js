const Validator = require('validator');
const isEmpty = require('../util/is_empty');

module.exports = function validateResetPasswordInput(data) {
  let errors = {};

  data.newPassword1 = !isEmpty(data.newPassword1) ? data.newPassword1 : '';
  data.newPassword2 = !isEmpty(data.newPassword2) ? data.newPassword2 : '';

  // password validation
  if(Validator.isEmpty(data.newPassword1)) {
    errors.newPassword1 = 'Password field required.';
  }
  if(!Validator.isLength(data.newPassword1, {min: 6, max: 30})){
    errors.newPassword1 = "Password must be between 6 and 30 characters."
  }

  // confirm password validation
  if(Validator.isEmpty(data.newPassword2)) {
    errors.newPassword2 = 'Confirm Password field required.';
  }
  if(!Validator.equals(data.newPassword1, data.newPassword2)) {
    errors.newPassword2 = 'Password must match.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
