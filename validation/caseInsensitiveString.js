/*
*
* This is a global function, used make passed string case insensitive
*
* */

module.exports = caseInsensitiveString = (string) => new RegExp(["^", string, "$"].join(""), "i");
