// import modules
const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name.trim() : '';
  data.email = !isEmpty(data.email) ? data.email.toLowerCase().trim() : '';
  data.password = !isEmpty(data.password) ? data.password : '';

  // name validation
  if(!Validator.isLength(data.name, {min: 2, max: 30})){
    errors.name = "Name must be in between 2 and 30 characters."
  }
  if(Validator.isEmpty(data.name)) {
    errors.name = 'Name/Company is required.';
  }

  // email validation
  if(!Validator.isEmail(data.email)) {
    errors.email = 'Invalid email....';
  }
  if(Validator.isEmpty(data.email)) {
    errors.email = 'Email is required.';
  }

  // password validation
  if(!Validator.isLength(data.password, {min: 6, max: 30})){
    errors.password = 'Password must be in between 6 and 30 characters.';
  }
  if(Validator.isEmpty(data.password)) {
    errors.password = 'Password is required.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}