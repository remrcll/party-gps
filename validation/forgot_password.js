const Validator = require('validator');
const isEmpty = require('../util/is_empty');

module.exports = function validateForgotPasswordInput(data) {
  let errors = {};

  data.email = !isEmpty(data.email) ? data.email : '';

  // email validation
  if(!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid.';
  }
  if(Validator.isEmpty(data.email)) {
    errors.email = 'Email field is required.';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
