/*
*
* This is a global function, comapares passed strings a , b and returns a boolean
*
* */

module.exports = caseInsensitiveBoolean = (a, b) => {
  return typeof a === 'string' && typeof b === 'string'
      ? a.localeCompare(b, undefined, { sensitivity: 'accent' }) === 0
      : a === b;
};
