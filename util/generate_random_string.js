/*
* This is a global function, used for
* generating a random string
* */
const secret = require('../config/keys').secretOrKey;

const generateRandomString = (size) => {
  let randomString = '';

  for (let i = 0; i < size; i++) {
    randomString += secret.charAt(Math.floor(Math.random() * secret.length));
  }
  return randomString;
}

module.exports = generateRandomString;
