/*
*
* This is a global function, which return an alret object
*
* */

module.exports = alert = (type, message) => {
  // alert respone object
  return {
    id: (new Date()).getTime(),
    type,
    message,
  }
};
