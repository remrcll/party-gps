// import packages
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');

// import files & others
const port = process.env.PORT || 5000;
const db = require('./config/keys').mongoURI;
const test = require('./api/v1/test/test');
const local_auth = require('./api/v1/auth/local/auth');
const spotify_auth = require('./api/v1/auth/spotify/auth');
const user = require('./api/v1/auth/user');
const reset = require('./api/v1/auth/reset');
const venue = require('./api/venue');
const venue_v1 = require('./api/v1/venue/venue');
const event = require('./api/event');
const event_v1 = require('./api/v1/event/event');
const artist = require('./api/artist');

// initialize express app
const app = express();

// body parser & cookie parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());

// connect to database
mongoose
  .connect(db, { useNewUrlParser: true, useCreateIndex: true, })
  .then(() => console.log('mongoDB connected successfully...'))
  .catch(err => console.log(err));

// passport middleware
app.use(passport.initialize());

// passport config
require('./config/passport')(passport);

// api routes
app.use('/test', test);
app.use('/auth/local', local_auth);
app.use('/auth/spotify', spotify_auth);
app.use('/user', user);
app.use('/reset', reset);
app.use('/api/venue', venue);
app.use('/v1/venue', venue_v1);
app.use('/api/event', event);
app.use('/v1/event', event_v1);
app.use('/api/artist', artist);

// server static assets if in production
if(process.env.NODE_ENV === 'production') {
  // set static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
} else {
  app.get('/', (req, res) => {
    res.status(200).json({response: `/ : server running on port ${port}`});
  });
}

// start server
const server = app.listen(port, () => console.log(`Server running on port: ${port}!`));

// Hey What's Up !!!
//                      ______
//                    <((((((\\\
//                    /      . }\
//                    ;--..--._|}
// (\                 '--/\--'  )
//  \\                | '-'  :'|
//   \\               . -==- .-|
//    \\               \.__.'   \--._
//    [\\          __.--|       //  _/'--.
//    \ \\       .'-._ ('-----'/ __/      \
//     \ \\     /   __>|      | '--.       |
//      \ \\   |   \   |     /    /       /
//       \ '\ /     \  |     |  _/       /
//        \  \       \ |     | /        /
//         \  \      \ |     |/        /
